<?php

Route::group([
    'middleware' => ['web', 'cacheredis'],
    'namespace' => 'Marketing\Routes',
    ], function ($router) {
        if (!Illuminate\Support\Facades\Schema::hasTable('pages')) {
            return;
        }

    // Check cache first
        $pages = Redis::get('results:pages:list');


        if ($pages != null) {
            $pages = unserialize($pages);
        }
        else {
            $pages = TCG\Voyager\Models\Page::all();
            Redis::set('results:pages:list', serialize($pages));
        }

        foreach ($pages as $page){

            if ($page->slug == 'home') {
                $router->get($page->slug, ['as' => $page->name, function() use ($page, $router){
                    return $this->app->call('App\Http\Controllers\PageController@showPage', [
                        'page' => $page,
                        'parameters' => $router->current()->parameters(),
                        ]);
                }]);
                $router->get('/', ['as' => $page->name, function() use ($page, $router){
                    return $this->app->call('App\Http\Controllers\PageController@showPage', [
                        'page' => $page,
                        'parameters' => $router->current()->parameters(),
                        ]);
                }]);
            }
            else {
                $router->get($page->slug, ['as' => $page->name, function() use ($page, $router){
                    return $this->app->call('App\Http\Controllers\PageController@showPage', [
                        'page' => $page,
                        'parameters' => $page->toArray(),
                        ]);
                }]);
            }

        }

    // Check cache first
        $posts = Redis::get('results:posts:list');

        if ($posts != null) {
            $posts = unserialize($posts);
        }
        else {
            $posts = TCG\Voyager\Models\Post::all();
            Redis::set('results:posts:list', serialize($posts));
        }


        foreach ($posts as $post){

            $router->get('/blog/'.$post->slug, ['as' => $post->name, function() use ($post, $router){
                return $this->app->call('App\Http\Controllers\PageController@showPost', [
                    'post' => $post,
                    'parameters' => $post->toArray(),
                    ]);
            }]);

        }
    });