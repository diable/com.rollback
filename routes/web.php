<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('login', function () {
//     return redirect('/admin/login');
// });

Auth::routes();


// Route::get('/', function () {
//     // return redirect('home');
//     // array( 'as' => 'home', 'uses' => 'HomeController@showWelcome' )
//     return view('home');

// });

//will be cached by Varnish
Route::group(['middleware' => 'cacheable'], function() {
    Route::get('/welcome', 'SearchController@index');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['middleware' => 'web'], function () {
    // Route::get('login', function () {
    //     return redirect('/admin/login');
    // });
    Route::auth();

    Route::delete('searches', 'SearchController@destroyAll');
    Route::resource('searches', 'SearchController', ['except' => ['edit']]);

    Route::get('searches/{searchId}/resources', 'ResourceController@download');
    Route::get('resources', 'ResourceController@downloadAll');

    Route::resource('backups', 'BackupController', ['except' => ['edit']]);
    Route::get('backups/{searchId}/start', 'BackupController@start');

    Route::get('websettings', 'WebsiteSettingController@index');
    Route::get('test', 'WebsiteSettingController@crawltest');
    Route::get('profile', 'UserController@showProfile');

    Route::get('/verifyemail/{token}', 'Auth\RegisterController@verify');

    Route::post('form', ['as' => 'form_store', 'uses' => 'FormsController@store']);

});

Route::group([
    'prefix' => 'api',
    'namespace' => 'Api',
    'middleware' => 'cors'
], function () {
    // user
    Route::get('users/{user_id}', 'UserController@show');

    // user's searches
    Route::get('users/{user_id}/searches', 'SearchController@index');
    Route::get('users/{user_id}/searches/{search_id}', 'SearchController@show');
    Route::patch('users/{user_id}/searches/{search_id}', 'SearchController@update');
    Route::delete('users/{user_id}/searches', 'SearchController@destroyAll');
    Route::delete('users/{user_id}/searches/{search_id}', 'SearchController@destroy');

    // user's searches's urls
    Route::get('users/{user_id}/searches/{search_id}/urls', 'UrlController@index');
    Route::get('users/{user_id}/searches/{search_id}/urls/{url_id}', 'UrlController@show');

    // user searches' resources
    Route::get('users/{user_id}/searches/{search_id}/resources', 'ResourceController@index');
    Route::get('users/{user_id}/searches/{search_id}/resources/{resource_id}', 'ResourceController@show');


    Route::get('users/{user_id}/urls', 'UrlController@showUserUrls');
    Route::get('users/{user_id}/resources', 'ResourceController@showUserResources');

    Route::get('users/{user_id}/backups', 'BackupController@index');
    Route::get('users/{user_id}/backups/{search_id}', 'BackupController@show');
    // user's backups's urls
    Route::get('users/{user_id}/backups/{search_id}/urls', 'BackupController@urls');


});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
