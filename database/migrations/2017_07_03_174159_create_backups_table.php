<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBackupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type', 255);
            $table->string('entrypoint', 1000);
            $table->string('remote_url', 1000);
            $table->integer('user_id')->unsigned()->index();
            $table->integer('search_id')->unsigned()->index();
            $table->boolean('finished')->default(false);
            $table->string('version', 25);
            $table->timestamps();
        });

        Schema::table('backups', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('search_id')->references('id')->on('searches')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('backups', function (Blueprint $table) {
            $table->dropForeign('backups_user_id_foreign');
            $table->dropForeign('backups_search_id_foreign');
        });
        Schema::drop('backups');
    }
}