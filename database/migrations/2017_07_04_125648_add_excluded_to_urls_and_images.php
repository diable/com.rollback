<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExcludedToUrlsAndImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('urls', function($table) {
            $table->boolean('excluded')->default(false);
        });
        Schema::table('images', function($table) {
            $table->boolean('excluded')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('urls', function($table) {
            $table->dropColumn('excluded');
        });
        Schema::table('images', function($table) {
            $table->dropColumn('excluded');
        });
    }
}
