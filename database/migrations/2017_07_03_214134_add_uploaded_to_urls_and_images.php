<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUploadedToUrlsAndImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('urls', function($table) {
            $table->boolean('uploaded')->default(false);
        });
        Schema::table('images', function($table) {
            $table->boolean('uploaded')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('urls', function($table) {
            $table->dropColumn('uploaded');
        });
        Schema::table('images', function($table) {
            $table->dropColumn('uploaded');
        });
    }
}
