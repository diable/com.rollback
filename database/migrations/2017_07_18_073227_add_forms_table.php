<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type', 255);
            $table->string('action_url', 1000);
            $table->string('page_url', 1000);
            $table->string('form_method', 25);
            $table->longText('form_payload');
            $table->longText('form_fields');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('search_id')->unsigned()->index();
            $table->boolean('synced')->default(false);
            $table->boolean('replied')->default(false);
            $table->boolean('active')->default(false);
            $table->string('version', 25);
            $table->timestamps();
        });

        Schema::table('forms', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('search_id')->references('id')->on('searches')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('forms', function (Blueprint $table) {
            $table->dropForeign('forms_user_id_foreign');
            $table->dropForeign('forms_search_id_foreign');
        });
        Schema::drop('forms');
    }
}
