# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.2.6-MariaDB)
# Database: php_crawler
# Generation Time: 2017-07-17 07:05:33 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table backups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `backups`;

CREATE TABLE `backups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entrypoint` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remote_url` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `search_id` int(10) unsigned NOT NULL,
  `finished` tinyint(1) NOT NULL DEFAULT 0,
  `version` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `backups_user_id_index` (`user_id`),
  KEY `backups_search_id_index` (`search_id`),
  CONSTRAINT `backups_search_id_foreign` FOREIGN KEY (`search_id`) REFERENCES `searches` (`id`) ON DELETE CASCADE,
  CONSTRAINT `backups_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`)
VALUES
	(1,NULL,1,'Category 1','category-1','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(2,NULL,1,'Category 2','category-2','2017-07-05 02:47:25','2017-07-05 02:47:25');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table data_rows
# ------------------------------------------------------------

DROP TABLE IF EXISTS `data_rows`;

CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `data_rows` WRITE;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`)
VALUES
	(1,1,'id','number','ID',1,0,0,0,0,0,'',1),
	(2,1,'author_id','text','Author',1,0,1,1,0,1,'',2),
	(3,1,'category_id','text','Category',1,0,1,1,1,0,'',3),
	(4,1,'title','text','Title',1,1,1,1,1,1,'',4),
	(5,1,'excerpt','text_area','excerpt',1,0,1,1,1,1,'',5),
	(6,1,'body','rich_text_box','Body',1,0,1,1,1,1,'',6),
	(7,1,'image','image','Post Image',0,1,1,1,1,1,'\n{\n    \"resize\": {\n        \"width\": \"1000\",\n        \"height\": \"null\"\n    },\n    \"quality\": \"70%\",\n    \"upsize\": true,\n    \"thumbnails\": [\n        {\n            \"name\": \"medium\",\n            \"scale\": \"50%\"\n        },\n        {\n            \"name\": \"small\",\n            \"scale\": \"25%\"\n        },\n        {\n            \"name\": \"cropped\",\n            \"crop\": {\n                \"width\": \"300\",\n                \"height\": \"250\"\n            }\n        }\n    ]\n}',7),
	(8,1,'slug','text','slug',1,0,1,1,1,1,'\n{\n    \"slugify\": {\n        \"origin\": \"title\",\n        \"forceUpdate\": true\n    }\n}',8),
	(9,1,'meta_description','text_area','meta_description',1,0,1,1,1,1,'',9),
	(10,1,'meta_keywords','text_area','meta_keywords',1,0,1,1,1,1,'',10),
	(11,1,'status','select_dropdown','status',1,1,1,1,1,1,'\n{\n    \"default\": \"DRAFT\",\n    \"options\": {\n        \"PUBLISHED\": \"published\",\n        \"DRAFT\": \"draft\",\n        \"PENDING\": \"pending\"\n    }\n}',11),
	(12,1,'created_at','timestamp','created_at',0,1,1,0,0,0,'',12),
	(13,1,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',13),
	(14,2,'id','number','id',1,0,0,0,0,0,'',1),
	(15,2,'author_id','text','author_id',1,0,0,0,0,0,'',2),
	(16,2,'title','text','title',1,1,1,1,1,1,'',3),
	(17,2,'excerpt','text_area','excerpt',1,0,1,1,1,1,'',4),
	(18,2,'body','rich_text_box','body',1,0,1,1,1,1,'',5),
	(19,2,'slug','text','slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\"}}',6),
	(20,2,'meta_description','text','meta_description',1,0,1,1,1,1,'',7),
	(21,2,'meta_keywords','text','meta_keywords',1,0,1,1,1,1,'',8),
	(22,2,'status','select_dropdown','status',1,1,1,1,1,1,'{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}',9),
	(23,2,'created_at','timestamp','created_at',1,1,1,0,0,0,'',10),
	(24,2,'updated_at','timestamp','updated_at',1,0,0,0,0,0,'',11),
	(25,2,'image','image','image',0,1,1,1,1,1,'',12),
	(26,3,'id','number','id',1,0,0,0,0,0,NULL,1),
	(27,3,'name','text','name',1,1,1,1,1,1,NULL,2),
	(28,3,'email','text','email',1,1,1,1,1,1,NULL,3),
	(29,3,'password','password','password',1,0,0,1,1,0,NULL,4),
	(30,3,'remember_token','text','remember_token',0,0,0,0,0,0,NULL,5),
	(31,3,'created_at','timestamp','created_at',0,1,1,0,0,0,NULL,6),
	(32,3,'updated_at','timestamp','updated_at',0,0,0,0,0,0,NULL,7),
	(33,3,'avatar','image','avatar',0,1,1,1,1,1,NULL,8),
	(34,5,'id','number','id',1,0,0,0,0,0,'',1),
	(35,5,'name','text','name',1,1,1,1,1,1,'',2),
	(36,5,'created_at','timestamp','created_at',0,0,0,0,0,0,'',3),
	(37,5,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',4),
	(38,4,'id','number','id',1,0,0,0,0,0,'',1),
	(39,4,'parent_id','select_dropdown','parent_id',0,0,1,1,1,1,'{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}',2),
	(40,4,'order','text','order',1,1,1,1,1,1,'{\"default\":1}',3),
	(41,4,'name','text','name',1,1,1,1,1,1,'',4),
	(42,4,'slug','text','slug',1,1,1,1,1,1,'',5),
	(43,4,'created_at','timestamp','created_at',0,0,1,0,0,0,'',6),
	(44,4,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',7),
	(45,6,'id','number','id',1,0,0,0,0,0,'',1),
	(46,6,'name','text','Name',1,1,1,1,1,1,'',2),
	(47,6,'created_at','timestamp','created_at',0,0,0,0,0,0,'',3),
	(48,6,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',4),
	(49,6,'display_name','text','Display Name',1,1,1,1,1,1,'',5),
	(50,1,'seo_title','text','seo_title',0,1,1,1,1,1,'',14),
	(51,1,'featured','checkbox','featured',1,1,1,1,1,1,'',15),
	(52,3,'role_id','text','role_id',0,1,1,1,1,1,NULL,9),
	(61,8,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),
	(62,8,'type','checkbox','Type',1,1,1,1,1,1,NULL,4),
	(63,8,'entrypoint','checkbox','Entrypoint',1,1,1,1,1,1,NULL,5),
	(64,8,'remote_url','checkbox','Remote Url',1,1,1,1,1,1,NULL,6),
	(65,8,'user_id','checkbox','User Id',1,1,1,1,1,1,NULL,3),
	(66,8,'search_id','checkbox','Search Id',1,1,1,1,1,1,NULL,2),
	(67,8,'finished','checkbox','Finished',1,1,1,1,1,1,NULL,7),
	(68,8,'version','checkbox','Version',1,1,1,1,1,1,NULL,8),
	(69,8,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,9),
	(70,8,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,10),
	(71,3,'email_token','checkbox','Email Token',0,1,1,1,1,1,NULL,16),
	(72,3,'last_login_ip','checkbox','Last Login Ip',0,1,1,1,1,1,NULL,13),
	(73,3,'last_login_at','timestamp','Last Login At',0,1,1,1,1,1,NULL,15),
	(74,3,'verified','checkbox','Verified',1,1,1,1,1,1,NULL,14),
	(75,3,'stripe_id','checkbox','Stripe Id',0,1,1,1,1,1,NULL,10),
	(76,3,'card_brand','checkbox','Card Brand',0,1,1,1,1,1,NULL,12),
	(77,3,'card_last_four','checkbox','Card Last Four',0,1,1,1,1,1,NULL,11),
	(78,3,'trial_ends_at','timestamp','Trial Ends At',0,1,1,1,1,1,NULL,17),
	(79,16,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),
	(80,16,'user_id','checkbox','User Id',1,1,1,1,1,1,NULL,2),
	(81,16,'name','checkbox','Name',1,1,1,1,1,1,NULL,3),
	(82,16,'stripe_id','checkbox','Stripe Id',1,1,1,1,1,1,NULL,4),
	(83,16,'stripe_plan','checkbox','Stripe Plan',1,1,1,1,1,1,NULL,5),
	(84,16,'quantity','checkbox','Quantity',1,1,1,1,1,1,NULL,6),
	(85,16,'trial_ends_at','timestamp','Trial Ends At',0,1,1,1,1,1,NULL,7),
	(86,16,'ends_at','timestamp','Ends At',0,1,1,1,1,1,NULL,8),
	(87,16,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,9),
	(88,16,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,10),
	(89,17,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),
	(90,17,'type','checkbox','Type',1,1,1,1,1,1,NULL,3),
	(91,17,'entrypoint','checkbox','Entrypoint',1,1,1,1,1,1,NULL,4),
	(92,17,'user_id','checkbox','User Id',1,1,1,1,1,1,NULL,2),
	(93,17,'domain_limit','checkbox','Domain Limit',1,1,1,1,1,1,NULL,5),
	(94,17,'finished','checkbox','Finished',1,1,1,1,1,1,NULL,6),
	(95,17,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,7),
	(96,17,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,8),
	(97,19,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),
	(98,19,'user_id','checkbox','User Id',1,1,1,1,1,1,NULL,3),
	(99,19,'backup_id','checkbox','Backup Id',1,1,1,1,1,1,NULL,2),
	(100,19,'active','checkbox','Active',1,1,1,1,1,1,NULL,4),
	(101,19,'config','checkbox','Config',1,1,1,1,1,1,NULL,5),
	(102,19,'maintenance_message','checkbox','Maintenance Message',1,1,1,1,1,1,NULL,6),
	(103,19,'maintenance_mode','checkbox','Maintenance Mode',1,1,1,1,1,1,NULL,7),
	(104,19,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,8),
	(105,19,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,9),
	(106,17,'sitemap','checkbox','Sitemap',1,1,1,1,1,1,NULL,9),
	(107,17,'robots','checkbox','Robots',1,1,1,1,1,1,NULL,10);

/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table data_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `data_types`;

CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `controller`, `description`, `generate_permissions`, `server_side`, `created_at`, `updated_at`)
VALUES
	(1,'posts','posts','Post','Posts','voyager-news','TCG\\Voyager\\Models\\Post','','',1,0,'2017-07-05 02:47:24','2017-07-05 02:47:24'),
	(2,'pages','pages','Page','Pages','voyager-file-text','TCG\\Voyager\\Models\\Page','','',1,0,'2017-07-05 02:47:24','2017-07-05 02:47:24'),
	(3,'users','users','User','Users','voyager-person','TCG\\Voyager\\Models\\User',NULL,NULL,1,0,'2017-07-05 02:47:24','2017-07-08 03:36:15'),
	(4,'categories','categories','Category','Categories','voyager-categories','TCG\\Voyager\\Models\\Category','','',1,0,'2017-07-05 02:47:24','2017-07-05 02:47:24'),
	(5,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu','','',1,0,'2017-07-05 02:47:24','2017-07-05 02:47:24'),
	(6,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role','','',1,0,'2017-07-05 02:47:24','2017-07-05 02:47:24'),
	(8,'backups','backups','Backup','Backups',NULL,'App\\Backup','BackupController',NULL,1,1,'2017-07-08 00:59:42','2017-07-08 18:53:38'),
	(16,'subscriptions','subscriptions','Subscription','Subscriptions',NULL,'App\\AppSubscription',NULL,NULL,1,0,'2017-07-08 09:22:52','2017-07-08 09:22:52'),
	(17,'searches','searches','Search','Searches',NULL,'App\\Search','SearchController',NULL,1,0,'2017-07-08 18:54:02','2017-07-15 19:35:25'),
	(19,'website_settings','website-settings','Website Setting','Website Settings',NULL,'App\\WebsiteSetting','WebsiteSettingController',NULL,1,0,'2017-07-08 19:02:55','2017-07-08 19:02:55');

/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table failed_jobs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `images`;

CREATE TABLE `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `crawled` tinyint(1) NOT NULL DEFAULT 0,
  `image_data` blob NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `search_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `uploaded` tinyint(1) NOT NULL DEFAULT 0,
  `excluded` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `images_name_index` (`name`),
  KEY `images_user_id_index` (`user_id`),
  KEY `images_search_id_index` (`search_id`),
  CONSTRAINT `images_search_id_foreign` FOREIGN KEY (`search_id`) REFERENCES `searches` (`id`) ON DELETE CASCADE,
  CONSTRAINT `images_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table jobs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jobs`;

CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_reserved_reserved_at_index` (`queue`,`reserved`,`reserved_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table menu_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menu_items`;

CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`)
VALUES
	(1,1,'Dashboard','/admin','_self','voyager-boat',NULL,NULL,1,'2017-07-05 02:47:24','2017-07-05 02:47:24',NULL,NULL),
	(2,1,'Media','/admin/media','_self','voyager-images',NULL,22,3,'2017-07-05 02:47:24','2017-07-08 09:14:56',NULL,NULL),
	(3,1,'Posts','/admin/posts','_self','voyager-news',NULL,22,2,'2017-07-05 02:47:24','2017-07-08 09:14:56',NULL,NULL),
	(4,1,'Users','/admin/users','_self','voyager-person',NULL,11,1,'2017-07-05 02:47:24','2017-07-08 09:14:44',NULL,NULL),
	(5,1,'Categories','/admin/categories','_self','voyager-categories',NULL,22,4,'2017-07-05 02:47:24','2017-07-08 09:14:54',NULL,NULL),
	(6,1,'Pages','/admin/pages','_self','voyager-file-text',NULL,22,1,'2017-07-05 02:47:24','2017-07-08 09:14:56',NULL,NULL),
	(7,1,'Roles','/admin/roles','_self','voyager-lock',NULL,11,2,'2017-07-05 02:47:24','2017-07-08 09:14:52',NULL,NULL),
	(8,1,'Tools','','_self','voyager-tools',NULL,NULL,4,'2017-07-05 02:47:24','2017-07-15 08:16:24',NULL,NULL),
	(9,1,'Menu Builder','/admin/menus','_self','voyager-list',NULL,8,1,'2017-07-05 02:47:24','2017-07-08 01:05:50',NULL,NULL),
	(10,1,'Database','/admin/database','_self','voyager-data',NULL,8,2,'2017-07-05 02:47:24','2017-07-08 01:05:50',NULL,NULL),
	(11,1,'Settings','/admin/settings','_self','voyager-settings',NULL,NULL,5,'2017-07-05 02:47:24','2017-07-15 08:16:24',NULL,NULL),
	(12,2,'Home','/','_self',NULL,'#000000',NULL,1,'2017-07-05 19:58:19','2017-07-08 10:01:12',NULL,''),
	(13,2,'Features','/features','_self',NULL,'#000000',NULL,2,'2017-07-05 19:58:51','2017-07-08 10:01:12',NULL,''),
	(14,3,'Searches','/searches','_self','voyager-bolt','#000000',NULL,15,'2017-07-06 21:47:44','2017-07-06 21:47:44',NULL,''),
	(15,3,'Backups','/backups','_self','voyager-bug','#000000',NULL,16,'2017-07-06 21:49:05','2017-07-06 21:49:05',NULL,''),
	(16,1,'Searches','/admin/searches','_self',NULL,'#000000',25,1,'2017-07-08 01:05:39','2017-07-15 19:34:18',NULL,''),
	(17,1,'Backups','/admin/backups','_self',NULL,'#000000',25,2,'2017-07-08 01:07:10','2017-07-15 08:18:07',NULL,''),
	(18,2,'Pricing','/pricing','_self',NULL,'#000000',NULL,3,'2017-07-08 08:56:30','2017-07-08 10:01:12',NULL,''),
	(19,2,'Contact','/contact','_self',NULL,'#000000',NULL,5,'2017-07-08 08:56:46','2017-07-08 10:01:12',NULL,''),
	(20,2,'Login','/admin/login','_self',NULL,'#000000',NULL,6,'2017-07-08 08:57:03','2017-07-08 10:01:12',NULL,''),
	(21,2,'Signup','/admin/signup','_self',NULL,'#000000',NULL,7,'2017-07-08 09:12:32','2017-07-08 10:01:12',NULL,''),
	(22,1,'Marketing','','_self',NULL,'#000000',NULL,3,'2017-07-08 09:13:55','2017-07-15 08:16:24',NULL,''),
	(23,1,'Subscriptions','/subscriptions','_self',NULL,'#000000',NULL,6,'2017-07-08 09:15:36','2017-07-15 08:16:24',NULL,''),
	(24,2,'Blog','/blog','_self',NULL,'#000000',NULL,4,'2017-07-08 10:01:01','2017-07-08 10:01:12',NULL,''),
	(25,1,'App','','_self',NULL,'#000000',NULL,2,'2017-07-15 08:16:09','2017-07-15 08:16:20',NULL,'');

/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table menus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menus`;

CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'admin','2017-07-05 02:47:24','2017-07-05 02:47:24'),
	(2,'marketing','2017-07-05 19:57:21','2017-07-05 19:57:21'),
	(3,'webapp','2017-07-06 21:43:57','2017-07-06 21:43:57');

/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`migration`, `batch`)
VALUES
	('2014_10_12_000000_create_users_table',1),
	('2014_10_12_100000_create_password_resets_table',1),
	('2016_02_19_080933_create_urls_table',1),
	('2016_02_19_081708_create_searches_table',1),
	('2016_02_19_082509_create_resources_table',1),
	('2016_02_19_085348_add_foreign_keys_to_resources_table',1),
	('2016_02_19_085532_add_foreign_keys_to_urls_table',1),
	('2016_02_19_085640_add_foreign_keys_to_searches_table',1),
	('2017_06_18_054000_add_html_to_userls',2),
	('2017_06_18_062502_create_jobs_table',3),
	('2017_06_18_062644_create_failed_jobs_table',4),
	('2017_07_02_154541_create_images_table',5),
	('2017_07_03_174159_create_backups_table',6),
	('2017_07_03_174828_add_foreign_keys_to_images_table',6),
	('2017_07_03_200112_create_settings_table',7),
	('2017_07_03_214134_add_uploaded_to_urls_and_images',7),
	('2017_07_04_084416_add_last_loginat_to_users_table',8),
	('2017_07_04_125648_add_excluded_to_urls_and_images',9),
	('2017_07_05_002510_add_email_token_to_users_table',9),
	('2016_01_01_000000_add_voyager_user_fields',10),
	('2016_01_01_000000_create_data_types_table',10),
	('2016_01_01_000000_create_pages_table',10),
	('2016_01_01_000000_create_posts_table',10),
	('2016_02_15_204651_create_categories_table',10),
	('2016_05_19_173453_create_menu_table',10),
	('2016_10_21_190000_create_roles_table',10),
	('2016_10_21_190000_create_settings_table',11),
	('2016_11_30_135954_create_permission_table',11),
	('2016_11_30_141208_create_permission_role_table',11),
	('2016_12_26_201236_data_types__add__server_side',11),
	('2017_01_13_000000_add_route_to_menu_items_table',11),
	('2017_01_14_005015_create_translations_table',11),
	('2017_01_15_000000_add_permission_group_id_to_permissions_table',11),
	('2017_01_15_000000_create_permission_groups_table',11),
	('2017_01_15_000000_make_table_name_nullable_in_permissions_table',11),
	('2017_03_06_000000_add_controller_to_data_types_table',11),
	('2017_04_21_000000_add_order_to_data_rows_table',11),
	('2017_03_30_224314_add_template_to_page_table',12),
	('2017_04_10_095457_add_page_name_for_routing_to_page_table',12),
	('2017_07_08_032702_add_cashier_to_laravel',13),
	('2017_07_11_011050_add_sitemap_to_search',14);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `template` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`, `template`, `name`)
VALUES
	(1,0,'Hello World','Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.','<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','pages/AAgCCnqHfLlRub9syUdw.jpg','hello-world','Yar Meta Description','Keyword1, Keyword2','ACTIVE','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL,NULL),
	(2,1,'Home',NULL,'<p>test</p>',NULL,'home',NULL,NULL,'ACTIVE','2017-07-07 08:09:13','2017-07-07 08:09:13',NULL,NULL),
	(3,1,'Pricing',NULL,'<p>&lt;section class=\"section\"&gt;</p>\r\n<p>&lt;div class=\"section-headlines text-center\"&gt;</p>\r\n<p>&lt;h2&gt;Pricing Plan&lt;/h2&gt;</p>\r\n<p>&lt;p&gt;Get this HTML template for ridiculously low price and give your awesome app promotion it deserves now.&lt;/p&gt;</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&nbsp;</p>\r\n<p>&lt;div class=\"row-fluid\"&gt;</p>\r\n<p>&lt;div class=\"pricing-table row-fluid text-center\"&gt;</p>\r\n<p>&lt;div class=\"span4\"&gt;</p>\r\n<p>&lt;div class=\"plan\"&gt;</p>\r\n<p>&lt;div class=\"plan-name\"&gt;</p>\r\n<p>&lt;h2&gt;Basic&lt;/h2&gt;</p>\r\n<p>&lt;p class=\"muted\"&gt;Perfect for small budget&lt;/p&gt;</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;div class=\"plan-price\"&gt;</p>\r\n<p>&lt;b&gt;$19&lt;/b&gt; / month</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;div class=\"plan-details\"&gt;</p>\r\n<p>&lt;div&gt;</p>\r\n<p>&lt;b&gt;Unlimited&lt;/b&gt; Download</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;div&gt;</p>\r\n<p>&lt;b&gt;Free&lt;/b&gt; Priority Shipping</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;div&gt;</p>\r\n<p>&lt;b&gt;Unlimited&lt;/b&gt; Warranty</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;div class=\"plan-action\"&gt;</p>\r\n<p>&lt;a href=\"#\" class=\"btn btn-block btn-large\"&gt;Choose Plan&lt;/a&gt;</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&nbsp;</p>\r\n<p>&lt;div class=\"span4\"&gt;</p>\r\n<p>&lt;div class=\"plan prefered\"&gt;</p>\r\n<p>&lt;div class=\"plan-name\"&gt;</p>\r\n<p>&lt;h2&gt;Standard&lt;/h2&gt;</p>\r\n<p>&lt;p class=\"muted\"&gt;Perfect for medium budget&lt;/p&gt;</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;div class=\"plan-price\"&gt;</p>\r\n<p>&lt;b&gt;$39&lt;/b&gt; / month</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;div class=\"plan-details\"&gt;</p>\r\n<p>&lt;div&gt;</p>\r\n<p>&lt;b&gt;Unlimited&lt;/b&gt; Download</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;div&gt;</p>\r\n<p>&lt;b&gt;Free&lt;/b&gt; Priority Shipping</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;div&gt;</p>\r\n<p>&lt;b&gt;Unlimited&lt;/b&gt; Warranty</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;div class=\"plan-action\"&gt;</p>\r\n<p>&lt;a href=\"#\" class=\"btn btn-success btn-block btn-large\"&gt;Choose Plan&lt;/a&gt;</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&nbsp;</p>\r\n<p>&lt;div class=\"span4\"&gt;</p>\r\n<p>&lt;div class=\"plan\"&gt;</p>\r\n<p>&lt;div class=\"plan-name\"&gt;</p>\r\n<p>&lt;h2&gt;Advance&lt;/h2&gt;</p>\r\n<p>&lt;p class=\"muted\"&gt;Perfect for large budget&lt;/p&gt;</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;div class=\"plan-price\"&gt;</p>\r\n<p>&lt;b&gt;$59&lt;/b&gt; / month</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;div class=\"plan-details\"&gt;</p>\r\n<p>&lt;div&gt;</p>\r\n<p>&lt;b&gt;Unlimited&lt;/b&gt; Download</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;div&gt;</p>\r\n<p>&lt;b&gt;Free&lt;/b&gt; Priority Shipping</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;div&gt;</p>\r\n<p>&lt;b&gt;Unlimited&lt;/b&gt; Warranty</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;div class=\"plan-action\"&gt;</p>\r\n<p>&lt;a href=\"#\" class=\"btn btn-block btn-large\"&gt;Choose Plan&lt;/a&gt;</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;p class=\"muted text-center\"&gt;Note: You can change or cancel your plan at anytime in your account settings.&lt;/p&gt;</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;/section&gt;</p>',NULL,'pricing',NULL,NULL,'ACTIVE','2017-07-08 09:35:51','2017-07-08 09:50:59',NULL,NULL),
	(4,1,'Features',NULL,'<h3 style=\"margin: 15px 0px; padding: 0px; font-size: 14px; color: #000000; font-family: \'Open Sans\', Arial, sans-serif;\">The standard Lorem Ipsum passage, used since the 1500s</h3>\r\n<p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: #000000; font-family: \'Open Sans\', Arial, sans-serif;\">\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>\r\n<h3 style=\"margin: 15px 0px; padding: 0px; font-size: 14px; color: #000000; font-family: \'Open Sans\', Arial, sans-serif;\">Section 1.10.32 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC</h3>\r\n<p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: #000000; font-family: \'Open Sans\', Arial, sans-serif;\">\"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\"</p>\r\n<h3 style=\"margin: 15px 0px; padding: 0px; font-size: 14px; color: #000000; font-family: \'Open Sans\', Arial, sans-serif;\">1914 translation by H. Rackham</h3>\r\n<p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: #000000; font-family: \'Open Sans\', Arial, sans-serif;\">\"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?\"</p>\r\n<h3 style=\"margin: 15px 0px; padding: 0px; font-size: 14px; color: #000000; font-family: \'Open Sans\', Arial, sans-serif;\">Section 1.10.33 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC</h3>\r\n<p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: #000000; font-family: \'Open Sans\', Arial, sans-serif;\">\"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.\"</p>\r\n<h3 style=\"margin: 15px 0px; padding: 0px; font-size: 14px; color: #000000; font-family: \'Open Sans\', Arial, sans-serif;\">1914 translation by H. Rackham</h3>',NULL,'features',NULL,NULL,'ACTIVE','2017-07-08 09:36:18','2017-07-16 23:12:05',NULL,NULL),
	(5,1,'Contact',NULL,NULL,NULL,'contact',NULL,NULL,'ACTIVE','2017-07-08 09:36:52','2017-07-08 09:36:52',NULL,NULL),
	(6,1,'Blog',NULL,NULL,NULL,'blog',NULL,NULL,'ACTIVE','2017-07-08 10:00:39','2017-07-08 10:00:39',NULL,NULL);

/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table permission_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permission_groups`;

CREATE TABLE `permission_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permission_groups_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table permission_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permission_role`;

CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;

INSERT INTO `permission_role` (`permission_id`, `role_id`)
VALUES
	(1,1),
	(2,1),
	(3,1),
	(4,1),
	(5,1),
	(6,1),
	(7,1),
	(8,1),
	(9,1),
	(10,1),
	(11,1),
	(12,1),
	(13,1),
	(14,1),
	(15,1),
	(16,1),
	(17,1),
	(18,1),
	(19,1),
	(20,1),
	(21,1),
	(22,1),
	(23,1),
	(24,1),
	(25,1),
	(26,1),
	(27,1),
	(28,1),
	(29,1),
	(30,1),
	(31,1),
	(32,1),
	(33,1),
	(34,1),
	(40,1),
	(40,2),
	(41,1),
	(41,2),
	(42,1),
	(42,2),
	(43,1),
	(43,2),
	(44,1),
	(44,2);

/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `permission_group_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`, `permission_group_id`)
VALUES
	(1,'browse_admin',NULL,'2017-07-05 02:47:24','2017-07-05 02:47:24',NULL),
	(2,'browse_database',NULL,'2017-07-05 02:47:24','2017-07-05 02:47:24',NULL),
	(3,'browse_media',NULL,'2017-07-05 02:47:24','2017-07-05 02:47:24',NULL),
	(4,'browse_settings',NULL,'2017-07-05 02:47:24','2017-07-05 02:47:24',NULL),
	(5,'browse_menus','menus','2017-07-05 02:47:24','2017-07-05 02:47:24',NULL),
	(6,'read_menus','menus','2017-07-05 02:47:24','2017-07-05 02:47:24',NULL),
	(7,'edit_menus','menus','2017-07-05 02:47:24','2017-07-05 02:47:24',NULL),
	(8,'add_menus','menus','2017-07-05 02:47:24','2017-07-05 02:47:24',NULL),
	(9,'delete_menus','menus','2017-07-05 02:47:24','2017-07-05 02:47:24',NULL),
	(10,'browse_pages','pages','2017-07-05 02:47:24','2017-07-05 02:47:24',NULL),
	(11,'read_pages','pages','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(12,'edit_pages','pages','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(13,'add_pages','pages','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(14,'delete_pages','pages','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(15,'browse_roles','roles','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(16,'read_roles','roles','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(17,'edit_roles','roles','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(18,'add_roles','roles','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(19,'delete_roles','roles','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(20,'browse_users','users','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(21,'read_users','users','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(22,'edit_users','users','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(23,'add_users','users','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(24,'delete_users','users','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(25,'browse_posts','posts','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(26,'read_posts','posts','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(27,'edit_posts','posts','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(28,'add_posts','posts','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(29,'delete_posts','posts','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(30,'browse_categories','categories','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(31,'read_categories','categories','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(32,'edit_categories','categories','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(33,'add_categories','categories','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(34,'delete_categories','categories','2017-07-05 02:47:25','2017-07-05 02:47:25',NULL),
	(40,'browse_backups','backups','2017-07-08 00:59:42','2017-07-08 00:59:42',NULL),
	(41,'read_backups','backups','2017-07-08 00:59:42','2017-07-08 00:59:42',NULL),
	(42,'edit_backups','backups','2017-07-08 00:59:42','2017-07-08 00:59:42',NULL),
	(43,'add_backups','backups','2017-07-08 00:59:42','2017-07-08 00:59:42',NULL),
	(44,'delete_backups','backups','2017-07-08 00:59:42','2017-07-08 00:59:42',NULL),
	(45,'browse_subscriptions','subscriptions','2017-07-08 09:22:52','2017-07-08 09:22:52',NULL),
	(46,'read_subscriptions','subscriptions','2017-07-08 09:22:52','2017-07-08 09:22:52',NULL),
	(47,'edit_subscriptions','subscriptions','2017-07-08 09:22:52','2017-07-08 09:22:52',NULL),
	(48,'add_subscriptions','subscriptions','2017-07-08 09:22:52','2017-07-08 09:22:52',NULL),
	(49,'delete_subscriptions','subscriptions','2017-07-08 09:22:52','2017-07-08 09:22:52',NULL),
	(50,'browse_searches','searches','2017-07-08 18:54:02','2017-07-08 18:54:02',NULL),
	(51,'read_searches','searches','2017-07-08 18:54:02','2017-07-08 18:54:02',NULL),
	(52,'edit_searches','searches','2017-07-08 18:54:02','2017-07-08 18:54:02',NULL),
	(53,'add_searches','searches','2017-07-08 18:54:02','2017-07-08 18:54:02',NULL),
	(54,'delete_searches','searches','2017-07-08 18:54:02','2017-07-08 18:54:02',NULL),
	(55,'browse_website_settings','website_settings','2017-07-08 19:02:56','2017-07-08 19:02:56',NULL),
	(56,'read_website_settings','website_settings','2017-07-08 19:02:56','2017-07-08 19:02:56',NULL),
	(57,'edit_website_settings','website_settings','2017-07-08 19:02:56','2017-07-08 19:02:56',NULL),
	(58,'add_website_settings','website_settings','2017-07-08 19:02:56','2017-07-08 19:02:56',NULL),
	(59,'delete_website_settings','website_settings','2017-07-08 19:02:56','2017-07-08 19:02:56',NULL);

/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`)
VALUES
	(1,1,1,'Lorem Ipsum Post',NULL,'This is the excerpt for the Lorem Ipsum Post','<p>This is the body of the lorem ipsum post</p>','posts/nlje9NZQ7bTMYOUG4lF1.jpg','lorem-ipsum-post','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',1,'2017-07-05 02:47:25','2017-07-16 21:04:31'),
	(2,0,NULL,'My Sample Post',NULL,'This is the excerpt for the sample Post','<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>','posts/7uelXHi85YOfZKsoS6Tq.jpg','my-sample-post','Meta Description for sample post','keyword1, keyword2, keyword3','PUBLISHED',0,'2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(3,0,NULL,'Latest Post',NULL,'This is the excerpt for the latest post','<p>This is the body for the latest post</p>','posts/9txUSY6wb7LTBSbDPrD9.jpg','latest-post','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',0,'2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(4,0,NULL,'Yarr Post',NULL,'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.','<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>','posts/yuk1fBwmKKZdY2qR1ZKM.jpg','yarr-post','this be a meta descript','keyword1, keyword2, keyword3','PUBLISHED',0,'2017-07-05 02:47:25','2017-07-05 02:47:25');

/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table resources
# ------------------------------------------------------------

DROP TABLE IF EXISTS `resources`;

CREATE TABLE `resources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `search_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `resources_user_id_index` (`user_id`),
  KEY `resources_search_id_index` (`search_id`),
  CONSTRAINT `resources_search_id_foreign` FOREIGN KEY (`search_id`) REFERENCES `searches` (`id`) ON DELETE CASCADE,
  CONSTRAINT `resources_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`)
VALUES
	(1,'admin','Administrator','2017-07-05 02:47:24','2017-07-05 02:47:24'),
	(2,'user','Normal User','2017-07-05 02:47:24','2017-07-05 02:47:24');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table searches
# ------------------------------------------------------------

DROP TABLE IF EXISTS `searches`;

CREATE TABLE `searches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entrypoint` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `domain_limit` tinyint(1) NOT NULL DEFAULT 1,
  `finished` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sitemap` longtext COLLATE utf8_unicode_ci NOT NULL,
  `robots` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `searches_user_id_index` (`user_id`),
  CONSTRAINT `searches_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`)
VALUES
	(1,'title','Site Title','Rollback Title','','text',1),
	(2,'description','Site Description','Rollback Site Description','','text',2),
	(3,'logo','Site Logo','','','image',3),
	(4,'admin_bg_image','Admin Background Image','','','image',9),
	(5,'admin_title','Admin Title','Rollback Admin Title','','text',4),
	(6,'admin_description','Admin Description','Rollback Admin Description','','text',5),
	(7,'admin_loader','Admin Loader','','','image',6),
	(8,'admin_icon_image','Admin Icon Image','','','image',7),
	(9,'google_analytics_client_id','Google Analytics Client ID','308885418481-hfp7t825n64groh0scmnk1hs1ndamdq8.apps.googleusercontent.com','','text',9);

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table subscriptions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subscriptions`;

CREATE TABLE `subscriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_plan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table translations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `translations`;

CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`)
VALUES
	(1,'data_types','display_name_singular',1,'pt','Post','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(2,'data_types','display_name_singular',2,'pt','Página','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(3,'data_types','display_name_singular',3,'pt','Utilizador','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(4,'data_types','display_name_singular',4,'pt','Categoria','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(5,'data_types','display_name_singular',5,'pt','Menu','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(6,'data_types','display_name_singular',6,'pt','Função','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(7,'data_types','display_name_plural',1,'pt','Posts','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(8,'data_types','display_name_plural',2,'pt','Páginas','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(9,'data_types','display_name_plural',3,'pt','Utilizadores','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(10,'data_types','display_name_plural',4,'pt','Categorias','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(11,'data_types','display_name_plural',5,'pt','Menus','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(12,'data_types','display_name_plural',6,'pt','Funções','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(13,'pages','title',1,'pt','Olá Mundo','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(14,'pages','slug',1,'pt','ola-mundo','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(15,'pages','body',1,'pt','<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(16,'menu_items','title',1,'pt','Painel de Controle','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(17,'menu_items','title',2,'pt','Media','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(18,'menu_items','title',3,'pt','Publicações','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(19,'menu_items','title',4,'pt','Utilizadores','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(20,'menu_items','title',5,'pt','Categorias','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(21,'menu_items','title',6,'pt','Páginas','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(22,'menu_items','title',7,'pt','Funções','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(23,'menu_items','title',8,'pt','Ferramentas','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(24,'menu_items','title',9,'pt','Menus','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(25,'menu_items','title',10,'pt','Base de dados','2017-07-05 02:47:25','2017-07-05 02:47:25'),
	(26,'menu_items','title',11,'pt','Configurações','2017-07-05 02:47:25','2017-07-05 02:47:25');

/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table urls
# ------------------------------------------------------------

DROP TABLE IF EXISTS `urls`;

CREATE TABLE `urls` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `crawled` tinyint(1) NOT NULL DEFAULT 0,
  `user_id` int(10) unsigned NOT NULL,
  `search_id` int(10) unsigned NOT NULL,
  `page_html` longtext COLLATE utf8_unicode_ci NOT NULL,
  `uploaded` tinyint(1) NOT NULL DEFAULT 0,
  `excluded` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `urls_name_index` (`name`),
  KEY `urls_user_id_index` (`user_id`),
  KEY `urls_search_id_index` (`search_id`),
  CONSTRAINT `urls_search_id_foreign` FOREIGN KEY (`search_id`) REFERENCES `searches` (`id`) ON DELETE CASCADE,
  CONSTRAINT `urls_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `verified` tinyint(4) NOT NULL DEFAULT 0,
  `stripe_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_brand` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `password`, `remember_token`, `email_token`, `last_login_ip`, `last_login_at`, `created_at`, `updated_at`, `verified`, `stripe_id`, `card_brand`, `card_last_four`, `trial_ends_at`)
VALUES
	(1,1,'test user','testuser@exampletestuser.com',NULL,'$2y$10$gg5V6Uiw3iB5szevEB.mCO2orXhytlRNlBIdPgCKL1Qy8xR84PoPu','Mcg49rDUhrYyAbYTuLIWATvcZhKKFO9xniOOqUTj5hZl0SAGKc44X6tr4ap3',NULL,'127.0.0.1','2017-07-16 08:30:53','2017-06-18 05:04:35','2017-07-16 08:30:53',0,NULL,NULL,NULL,NULL),
	(2,NULL,'test user','testuser2@example.com',NULL,'$2y$10$7h6XJrAsPHLRGS.kCloCQOozmbXSrhjVDhVLXSKEkOf194gZfpukC',NULL,NULL,'127.0.0.1','2017-07-04 18:01:16','2017-07-04 18:01:15','2017-07-04 18:01:16',0,NULL,NULL,NULL,NULL),
	(3,NULL,'test','testuser2@exampletestuser.com',NULL,'$2y$10$cOens1qrwz70JBLRLW3eJuAZ846Kyka.rJpApm9.fj0Pb7TzKPRRm',NULL,NULL,NULL,NULL,'2017-07-04 18:06:03','2017-07-04 18:06:03',0,NULL,NULL,NULL,NULL),
	(4,NULL,'test','testuser3@exampletestuser.com',NULL,'$2y$10$7tAVpzkrIcWVcik8hxh8zekP0EdacWzqR.L0ydPzM2YlN5ZFnJw8i',NULL,NULL,NULL,NULL,'2017-07-04 18:07:04','2017-07-04 18:07:04',0,NULL,NULL,NULL,NULL),
	(5,NULL,'test','testuser4@exampletestuser.com',NULL,'$2y$10$BFy46f4A65vQcLG0EC.1DeREk7X3mqwv8b7TzVyB7PNyxUpvjdHtq',NULL,NULL,NULL,NULL,'2017-07-04 18:27:00','2017-07-04 18:27:00',0,NULL,NULL,NULL,NULL),
	(6,NULL,'test','testuser5@exampletestuser.com',NULL,'$2y$10$SF1ce6JIgRiWxN.IeQjGn.qMJWYwGZYJ0s/LNrXDjM1j4Z5TQRVfu',NULL,NULL,NULL,NULL,'2017-07-04 18:28:23','2017-07-04 18:28:23',0,NULL,NULL,NULL,NULL),
	(7,NULL,'test','testuser6@exampletestuser.com',NULL,'$2y$10$CUd9gyRPH0gx0lxuL2Hqgu0FKfViM11uye2EjTB5kBGeLdYTLGaB2',NULL,NULL,NULL,NULL,'2017-07-04 18:29:04','2017-07-04 18:29:04',0,NULL,NULL,NULL,NULL),
	(8,2,'test','testuser7@exampletestuser.com',NULL,'$2y$10$gg5V6Uiw3iB5szevEB.mCO2orXhytlRNlBIdPgCKL1Qy8xR84PoPu',NULL,'dGVzdHVzZXI3QGV4YW1wbGV0ZXN0dXNlci5jb20=','127.0.0.1','2017-07-05 20:30:26','2017-07-05 00:42:09','2017-07-05 20:30:26',0,NULL,NULL,NULL,NULL),
	(9,NULL,'rt','testuser8@exampletestuser.com',NULL,'$2y$10$.NRRIfd50invKKVddtYlAuaaehkLeuObrWLgk43UJYwtKKFkNwX9y',NULL,'dGVzdHVzZXI4QGV4YW1wbGV0ZXN0dXNlci5jb20=',NULL,NULL,'2017-07-05 00:57:20','2017-07-05 00:57:20',0,NULL,NULL,NULL,NULL),
	(10,NULL,'rt','testuser9@exampletestuser.com',NULL,'$2y$10$gH/NJlEHVopGSIjgWihw7OqkA1ODYHtXO70MToPDHju99rLyyLY4q',NULL,'dGVzdHVzZXI5QGV4YW1wbGV0ZXN0dXNlci5jb20=',NULL,NULL,'2017-07-05 00:59:38','2017-07-05 01:02:02',1,NULL,NULL,NULL,NULL),
	(11,NULL,'test','testuser10@exampletestuser.com',NULL,'$2y$10$DhoTegZVNyIyfWV27tuufOQ59Sphkp8kM.g6v46JFglx8GFpau0qG',NULL,'dGVzdHVzZXIxMEBleGFtcGxldGVzdHVzZXIuY29t',NULL,NULL,'2017-07-05 01:06:18','2017-07-05 01:06:48',1,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table website_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `website_settings`;

CREATE TABLE `website_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `backup_id` int(10) unsigned NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `config` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `maintenance_message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `maintenance_mode` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `settings_user_id_index` (`user_id`),
  KEY `settings_backup_id_index` (`backup_id`),
  CONSTRAINT `settings_backup_id_foreign` FOREIGN KEY (`backup_id`) REFERENCES `backups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `settings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
