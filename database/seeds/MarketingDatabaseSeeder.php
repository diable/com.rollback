<?php

use Illuminate\Database\Seeder;

class MarketingDatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('ExtraPagesDataRowsTableSeeder');
        $this->call('MarketingPagesTableSeeder');
    }
}
