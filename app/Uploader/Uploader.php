<?php

namespace App\Uploader;

use App\Search;
use Aws\S3\S3Client;

class Uploader
{


    public function __construct()
    {
    }

    /**
     * Run the crawler until the search is finished or until
     * the search is deleted.
     *
     * @param \App\Search $search
     * @return bool
     */
    public function run(Search $search)
    {
        $pages = Search::find($search->id)
        ->urls()
        ->where(['crawled' => true])
        ->get()
        ->unique();


        $images = Search::find($search->id)
        ->images()
        ->where(['crawled' => true])
        ->get()
        ->unique();
        
        // TODO add search field to backup;
        // Can't determine which backup to upload;
        $backup = Search::find($search->id)->backups()->first();
 
        $domain = $search->entrypoint;

        $bucket = getS3BucketUrl($domain);


        $res = array();
        foreach ($pages as $key => $page) {
            $sanitize_domain = trim($page->name);
            $path = str_replace($domain, '', $sanitize_domain);
            if ($path == "") {
                $path = "index.html";
            }
            // if ($this->endsWith($path, "/")) {
            //     $path = trim($path, "/");
            // }
            $res['pages'][$path] = $page;
            
        }
        foreach ($images as $key => $image) {
           
            $sanitize_domain = trim($image->name);
            $path = str_replace($domain, '', $sanitize_domain);
            $res['img'][$path] = $image;
            
        }
       
        $aws_config = config('aws');


        /*1. Instantiate the client.*/
        $s3 = S3Client::factory($aws_config);

        if (count($res['pages']) > 0) {
            foreach ($res['pages'] as $key => $page) {
                $parts = explode( '?', $key ); 
                $filename = $parts[0];
                echo "Creating a new page with key {$filename}\n";
                sleep(5);
                $s3->putObject([
                    'Bucket' => $bucket,
                    'Key'    => $filename,
                    'Body'   => str_replace($domain, $bucket_url, $page->page_html),
                    'ACL' => 'public-read',
                    'ContentType' => 'text/html',
                    ]);
                $page->uploaded = true;
                $page->save();
            }
        }
        
        if (count($res['img']) > 0) {
            foreach ($res['img'] as $key => $page) {
                $parts = explode( '?', $key ); 
                $filename = $parts[0];
                echo "Creating a new image with key {$filename}\n";
                $s3->putObject([
                    'Bucket' => $bucket,
                    'Key'    => $filename,
                    'Body'   => $page->image_data,
                    'ACL' => 'public-read',
                    'ContentType' => 'image/png',
                    ]);
                $page->uploaded = true;
                $page->save();
            }
        }
        
    }

}