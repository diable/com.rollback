<?php

namespace App\Helpers;


class My {

	static public function voyagerTableObject($str) {
        $dataType['server_side'] = false;
        $dataType['display_name_plural'] = ucfirst($str);
        $dataType['display_name_singular'] = ucfirst($str);
        $dataType['name'] =  ucfirst($str);
        $dataType['slug'] =  $str;
        $dataType['icon'] =  $str;
        return (object) $dataType;
    }

    static public function getS3BucketUrl($domain) {
        $clean_domain = trim($domain, '/');
        $clean_domain = str_replace(array('http://', 'https://'), '', $clean_domain);

        $split_domain = explode('.', $clean_domain);

        $bucket = $split_domain[2].".".$split_domain[1].".".$split_domain[0];
        $bucket = uniqid($bucket, true);
        return $bucket;
    }

    static public function getBucketFromDomain($domain) {
        $bucket = str_replace(array('http://', 'https://', '.s3-website-us-west-2.amazonaws.com/'), '', $domain);
        return $bucket;
    }
    
    static public function getS3WebsiteUrl($bucket) {
        $bucket_url = "http://".$bucket.".s3-website-us-west-2.amazonaws.com/";
        return $bucket_url;
    }
}

?>