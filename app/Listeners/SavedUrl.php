<?php

namespace App\Listeners;

use App\Events\Url;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SavedUrl
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Url  $event
     * @return void
     */
    public function handle(Url $event)
    {
        //
    }
}
