<?php

namespace App\Listeners;

use App\Events\CrawlerIsDone;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Search;
use Mail;
use App\User;
use App\Mail\StartedCrawl;
use Log;

class CrawlerIsDoneEventWasFired
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  CrawlerIsDone  $event
     * @return void
     */
    public function handle(CrawlerIsDone $event)
    {
        Log::info("CrawlerIsDoneEventWasFired Begins");
        $search = $event->search;
        
        exec('cd ' . base_path() . ' && php artisan uploader:send ' . $search->id . ' > /dev/null &');
        $user = User::find($search->user_id)->toArray();
        // $search = $search->toArray();
        $email = new StartedCrawl($search);
        // Test email address
        // Mail::to($this->user->email)->send($email);
        Mail::to('c817f3b70d-185cb6@inbox.mailtrap.io')->send($email);
        Log::info("CrawlerIsDoneEventWasFired Ends");
    }
}
