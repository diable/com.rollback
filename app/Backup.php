<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;

class Backup extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'backups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type', 'entrypoint', 'remote_url', 'user_id', 'domain_limit', 'finished', 'version'];

    /**
     * Get the user that owns the backup.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the search that owns the backup.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function search()
    {
        return $this->belongsTo(Search::class);
    }

}
