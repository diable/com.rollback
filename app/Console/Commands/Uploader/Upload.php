<?php

namespace App\Console\Commands\Uploader;

use App\Search;
use App\Uploader\Uploader;
use Illuminate\Console\Command;

class Upload extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uploader:send {search_id : The unique id of the search}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start uploading site';

    /**
     * The search Eloquent model.
     *
     * @var \App\Search
     */
    protected $search;

    /**
     * @var \App\Uploader\Upload
     */
    protected $uploader;

    /**
     * Create a new command instance.
     *
     * @param \App\Uploader\Upload $uploader
     */
    public function __construct(Uploader $uploader)
    {
        parent::__construct();

        $this->uploader = $uploader;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->search = Search::find($this->argument('search_id'));

        if ($this->search === null) {
            return false;
        }

        return $this->uploader->run($this->search);
    }
}
