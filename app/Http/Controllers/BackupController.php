<?php

namespace App\Http\Controllers;

use App\Backup;

use App\User;
use App\Search;
use App\Url;

use App\Http\Controllers\Controller;
use Aws\S3\S3Client;
use App\Transformers\UrlTransformer;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class BackupController extends Controller
{

    /**
     * @var \Illuminate\Support\Facades\Auth
     */
    protected $user;

    /**
     * SearchController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Stopped working after upgrade to 5.4
        // $this->user = Auth::user();

        $this->middleware(function ($request, $next) { 
            $this->user = Auth::user();
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataType = \App\Helpers\My::voyagerTableObject("backups");

        return response()->view('backups.index', [
            'backups' => $this->user->backups,
            'dataType' => $dataType
        ]);

    }

    function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $search = Search::find($id);
        
        return response()->view('backups.show', [
            'backup' => $search->backups()->first(),
            'search' => $search,
            'urls' => $search->urls()->get()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    function start($id) {

        $search = Search::find($id);

        exec('cd ' . base_path() . ' && php artisan uploader:send ' . $search->id . ' > /dev/null &');

        //return redirect()->route('backups.index');
        return redirect('backups/' . $search->id);
    }

    function delete()  {
        $result = $s3->deleteBucketWebsite(array(
            'Bucket' => $bucket,
            ));
    }

    function verifyS3() {
        $result = $s3->getBucketWebsite(array(
            'Bucket' => $bucket,
            ));
        echo $result->getPath('IndexDocument/Suffix');
    }

}

/*'CacheControl' => '<string>',*/
/*'ContentDisposition' => '<string>',*/
/*'ContentEncoding' => '<string>',*/
/*'ContentLanguage' => '<string>',*/
/*'ContentLength' => <integer>,*/
/*'ContentSHA256' => '<string>',*/
/*'ContentType' => '<string>',*/
/*'Expires' => <integer || string || DateTime>,*/
/*'GrantFullControl' => '<string>',*/
/*'GrantRead' => '<string>',*/
/*'GrantReadACP' => '<string>',*/
/*'GrantWriteACP' => '<string>',*/
/*'Metadata' => ['<string>', ...],*/
/*'RequestPayer' => 'requester',*/
/*'SSECustomerAlgorithm' => '<string>',*/
/*'SSECustomerKey' => '<string>',*/
/*'SSECustomerKeyMD5' => '<string>',*/
/*'SSEKMSKeyId' => '<string>',*/
/*'ServerSideEncryption' => 'AES256|aws:kms',*/
/*'SourceFile' => '<string>',*/
/*'StorageClass' => 'STANDARD|REDUCED_REDUNDANCY|STANDARD_IA',*/
/*'Tagging' => '<string>',*/
        /*'WebsiteRedirectLocation' => '<string>',*/