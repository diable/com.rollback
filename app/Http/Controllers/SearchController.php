<?php

namespace App\Http\Controllers;

use App\Search;
use App\Backup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redis;
use Illuminate\Auth\Events\Registered;
use App\Jobs\JobCrawler;
use Log;
use Aws\S3\S3Client;


class SearchController extends Controller
{
    /**
     * @var \Illuminate\Support\Facades\Auth
     */
    protected $user;

    /**
     * SearchController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');

        // Stopped working after upgrade to 5.4
        // $this->user = Auth::user();

        $this->middleware(function ($request, $next) { 
            $this->user = Auth::user();
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataType = \App\Helpers\My::voyagerTableObject("searches");

        return response()->view('searches.index', [
            'searches' => $this->user->searches,
            'dataType' => $dataType
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Check cache first
        $search = Redis::get('search:show:search'.$id);
        $stats = Redis::get('search:show:searchstats'.$id);


        if ($search != null) {
            $result['search'] = unserialize($search);
        }
        else {
            $search = Search::find($id);
            $result['search'] = $search;
            Redis::set('search:show:search'.$id, serialize($search));
        }

        if ($stats != null) {
            $result['stats'] = unserialize($stats);
        }
        else {
            $stats = $this->getSearchStats($search);
            $result['stats'] = $stats;
            Redis::set('search:show:searchstats'.$id, serialize($stats));
        }

        return response()->view('searches.show', $result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('searches.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'entrypoint' => 'required|url|max:1000',
            'type' => 'required|in:email,phone,url'
            ]);

        if ($validator->fails()) {
            return back();
        }

        $search = new Search();
        $search->entrypoint = $request->entrypoint;
        $search->type = $request->type;
        $this->user->searches()->save($search);

        $backup = new Backup();
        $backup->entrypoint = $request->entrypoint;
        $backup->search_id = $search->id;
        $this->user->backups()->save($backup);

        Log::info("php artisan crawler:crawl Job Crawler started");
        // $this->dispatch(new JobCrawler($search));

        $domain = $search->entrypoint;

        $bucket = \App\Helpers\My::getS3BucketUrl($domain);
        $bucket_url = \App\Helpers\My::getS3WebsiteUrl($bucket);

        $aws_config = config('aws');
        /*1. Instantiate the client.*/
        $s3 = S3Client::factory($aws_config);

        echo "Creating bucket named {$bucket}\n";
        $created_bucker = $s3->createBucket(
            ['ACL' => 'public-read',
            'Bucket' => $bucket, 
            'CreateBucketConfiguration' => [
            'LocationConstraint' => 'us-west-2',
            ]

            ]);

        $s3->waitUntil('BucketExists', ['Bucket' => $bucket]);
        if ($created_bucker == true) {
           $backup->remote_url = $bucket_url;
           $backup->save();
       }

       $result = $s3->putBucketVersioning([
        'Bucket' => $bucket, 
        'VersioningConfiguration' => [ 
        'Status' => 'Enabled',
        ],
        ]);

       $key = 'error.html';
       echo "Creating a new object with key {$key}\n";
       $s3->putObject([
        'Bucket' => $bucket,
        'Key'    => $key,
        'Body'   => "Error!",
        'ACL' => 'public-read',
        'ContentType' => 'text/html'
        ]);

       /*2. Add website configuration.*/
       $result = $s3->putBucketWebsite(array(
        'Bucket'        => $bucket,    
        /*'ContentMD5' => '<string>',*/
        'WebsiteConfiguration' => [ 
        'ErrorDocument' => [
        'Key' => 'error.html', 
        ],
        'IndexDocument' => [
        'Suffix' => 'index.html', 
        ]
        ]
        ));


        exec('cd ' . base_path() . ' && php artisan crawler:crawl ' . $search->id . ' > /dev/null &');

       return redirect('searches/' . $search->id);
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'finished' => 'required|boolean'
            ]);

        if ($validator->fails()) {
            return back();
        }

        $search = Search::find($id);
        $search->finished = $request->finished;
        $search->save();

        return back();
    }

    /**
     * Remove the specified user's search.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Search::destroy($id);

        return redirect('searches');
    }

    /**
     * Remove all the user's searches.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroyAll()
    {
        $this->user->searches()->delete();

        return redirect('searches');
    }

    /**
     * Get search's statistics.
     *
     * @param \App\Search $search
     * @return array
     */
    private function getSearchStats(Search $search)
    {
        $total = $search->urls()->count();
        $crawled = $search->urls()->where(['crawled' => true])->count();

        if ($total > 0) {
            return [
            'percentCrawled' => round(($crawled  / $total) * 100),
            'percentNotCrawled' => round(100 - (($crawled  / $total) * 100))
            ];
        }

        return [
        'percentCrawled' => 0,
        'percentNotCrawled' => 0
        ];
    }
}
