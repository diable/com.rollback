<?php

namespace App\Http\Controllers;

use App\WebsiteSetting;
use App\User;
use App\Url;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use JavaScript;
use Log;
use Mail;
use Illuminate\Auth\Events\Registered;
use App\Jobs\ProcessUrl;
use App\Events\SavedUrl;

use App\React\ReactCrawler;
use t1gor\RobotsTxtParser\Source\RobotsTxtParser;

class WebsiteSettingController extends Controller
{
    /**
     * @var \Illuminate\Support\Facades\Auth
     */
    protected $user;

    /**
     * SearchController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');

        // Stopped working after upgrade to 5.4
        // $this->user = Auth::user();

        $this->middleware(function ($request, $next) { 
            $this->user = Auth::user();
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        
        JavaScript::put([
            'foo' => 'bar',
            'user' => $this->user,
            'settings' => $this->user->settings()->get()->toArray()
        ]);
        return response()->view('settings.index', [
        ]);
    }

    // TODO
    // Async curl
    public function crawltest() {
        $url = Url::find(11423);
        
        event($url);
        dispatch(new ProcessUrl($url));

       

    }
}
