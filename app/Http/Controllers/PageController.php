<?php

namespace App\Http\Controllers;

use TCG\Voyager\Models\Page;
use TCG\Voyager\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

class PageController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param Page $page
     * @param array $parameters
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function showPage(Page $page, array $parameters)
    {
        if ($page->slug == 'home') {
            $page->template = 'home';
        }
        elseif ($page->slug == 'blog') {
            $page->template = 'blog';
        }
        else {
            $page->template = 'page';
        }
  
        $this->prepareTemplate($page, $parameters);
        
        return view('marketing.marketing', compact('page'));
    }

    /**
     * Display the specified resource.
     *
     * @param Post $post
     * @param array $parameters
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function showPost(Post $post, array $parameters)
    {
        $post->template = 'blog.post';
        $this->prepareTemplate($post, $parameters);
        $page = $post;
        return view('marketing.marketing', compact('page'));
    }

    protected function prepareTemplate($page, array $parameters)
    {

        $templates = config('marketing.templates');

        if (! $page->template || ! isset($templates[$page->template])) {
            return;
        }
        $template = app($templates[$page->template]);

        $view = $template->getView();
            
        if (! view()->exists($view)) {
            return;
        }

        $template->prepare($view = view($view), $parameters);

        $page->view = $view;
    }
}
