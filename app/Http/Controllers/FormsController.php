<?php

namespace App\Http\Controllers;
use App\Form;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class FormsController extends Controller
{
    public function create()
    {
        return view('contact');
    }

    public function store(Request $request)
    {

    	$form = new Form();
        $form->form_payload = json_encode($request->all());
        $form->save();

        // \Mail::send('emails.contact',
        //     array(
        //         'name' => $request->get('name'),
        //         'email' => $request->get('email'),
        //         'user_message' => $request->get('message')
        //     ), function($message)
        // {
        //     $message->from('testemail@mail.com');
        //     $message->to('testemail@mail.com', 'Admin')->subject('Website Feedback');
        // });

      // return \Redirect::route('home')->with('message', 'Thanks for contacting us!');
        return redirect('/');

    }
}
