<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redis;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

	/**
     * @var \Illuminate\Support\Facades\Auth
     */
    protected $user;

    /**
     * SearchController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Stopped working after upgrade to 5.4
        // $this->user = Auth::user();

        $this->middleware(function ($request, $next) { 
            $this->user = Auth::user();
            return $next($request);
        });
    }

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function showProfile()
    {
    	// // Check cache first
     //    $page = Redis::get('user:profile:'.$this->user->id);
     //    if ($page != null) {
     //        return dd($page);
     //    }

     //    //Get from the database
     //    Redis::set('user:profile:', $this->user->id);

        return response()->view('profile.index', [
            'user' => $this->user
            ]);

    	
        // return view('user.profile', ['user' => $user]);
    }
}