<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Search;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Transformers\BackupTransformer;
use App\Transformers\UrlLimitedTransformer;
use Illuminate\Support\Facades\Validator;
use App\Transformers\SearchTransformer;

class BackupController extends ApiController
{
    /**
     * Get all the user's searches.
     *
     * @param int $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($userId)
    {
        $user = User::find($userId);;
        $backups =    $user->backups()->remember(10)
            // ->with(['urlsCount', 'crawledUrlsCount', 'resourcesCount'])
            ->get()
            ;

        return $this->respondWithCollection($backups, new BackupTransformer(), 'backups');
    }

    function urls($userId, $searchId) {
        $urls = array();
        
        $search = Search::find($searchId);

        $pages = $search
        ->urls()
        ->get()
        ->unique();

        $images = $search
        ->images()
        ->get()
        ->unique();

        foreach ($pages as $key => $page) {
            $urls[] = $page;
        }

        foreach ($images as $key => $image) {
            $urls[] = $image;
        }

        if ($urls) {
            return $this->respondWithCollection($urls, new UrlLimitedTransformer(), 'urls');
        }

        return $this->errorNotFound();
    }

    /**
     * Get one user's search.
     *
     * @param int $userId
     * @param int $searchId
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($userId, $searchId)
    {
        $search = User::find($userId)
            ->searches()
            ->with(['urlsCount' => function ($q) { $q->remember(10); }, 'crawledUrlsCount' => function ($q) { $q->remember(10); }, 'resourcesCount' => function ($q) { $q->remember(10); }])
            ->find($searchId);

        $pages = $search
        ->urls()
        ->get()
        ->unique();

        $images = $search
        ->images()
        ->get()
        ->unique();

        // Get pages without page_html
        foreach ($pages as $key => $page) {
            unset($page->page_html);
            $urls[] = $page;
        }

        foreach ($images as $key => $image) {
            unset($page->image_data);
            $urls[] = $image;
        }

        $backup = $search->backups()->get()[0];
        $backup->urls = $urls;
        if ($backup) {
            return $this->respondWithItem($backup, new BackupTransformer(), 'backups');
        }

        return $this->errorNotFound();
    }

    /**
     * Store a user's search.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'entrypoint' => 'required|url|max:1000',
            'type' => 'required|in:email,phone,url'
        ]);

        if (!$validator->fails()) {
            $search = User::find(Auth::user()->id)
                ->searches()
                ->create($request->all());

            exec('cd ' . base_path() . ' && php artisan crawler:crawl ' . $search->id . ' > /dev/null &');

            return $this->respondWithCreated('Search has been created.');
        }

        return $this->errorWrongArgs($validator->errors()->first());
    }

    /**
     * Update a user's search: mark it as finished.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $userId
     * @param int $searchId
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $userId, $searchId)
    {
        $validator = Validator::make($request->all(), [
            'finished' => 'required|boolean'
        ]);

        if (!$validator->fails()) {
            User::find($userId)
                ->searches()
                ->find($searchId)
                ->update(['finished' => $request->input('finished')]);

            return $this->respondWithCreated('The search has been stopped.');
        }

        return $this->errorWrongArgs($validator->errors()->first());
    }

    /**
     * Remove the specified user's search.
     *
     * @param  int $userId
     * @param  int $searchId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($userId, $searchId)
    {
        User::find($userId)
            ->searches()
            ->find($searchId)
            ->update(['finished' => true]);

        // give the crawler the time to finish the current crawled page
        sleep(1);

        User::find($userId)
            ->searches()
            ->find($searchId)
            ->delete();

        return $this->respondWithCreated('The search has been deleted.');
    }

    /**
     * Remove all the user's searches.
     *
     * @param  int $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyAll($userId)
    {
        User::find($userId)
            ->searches()
            ->update(['finished' => true]);

        // give the crawler the time to finish the current crawled page
        sleep(1);

        User::find($userId)
            ->searches()
            ->delete();

        return $this->respondWithCreated('All searches have been deleted.');
    }
}
