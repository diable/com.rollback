<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use App\Search;
use App\Crawler\Crawler;

class JobCrawler implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $user;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start a search crawl';

    /**
     * The search Eloquent model.
     *
     * @var \App\Search
     */
    protected $search;

    /**
     * @var \App\Crawler\Crawler
     */
    protected $crawler;

    /**
     * Create a new command instance.
     *
     * @param \App\Search $search
     */
    public function __construct($search)
    {
        $this->search = $search;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Crawler $crawler)
    {
        // TODO queued crawled doesnt work
        Log::info("Inside JobCrawler Begins");
        Log::info(var_export($this->search, true));

        if ($this->search === null) {
            Log::info("Inside JobCrawler: Search is null");
            return false;
        }
        Log::info("Inside JobCrawler Begins: " . $this->search->id);
        //exec('cd ' . base_path() . ' && php artisan crawler:crawl ' . $this->search->id . ' > /dev/null &');

        // $crawler->run($this->search);
        Log::info("Inside JobCrawler Ends");

    }
}
