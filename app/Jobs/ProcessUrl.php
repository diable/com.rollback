<?php

namespace App\Jobs;

use App\Url;
use App\Search;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
Use Log;

use t1gor\RobotsTxtParser\Source\RobotsTxtParser;
use vipnytt\UserAgentParser\UserAgentParser;

use JonnyW\PhantomJs\Client;
use Aws\S3\S3Client;

class ProcessUrl implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;


    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 120;

    protected $url;
    protected $search;
    protected $backup;

    /**
     * Create a new job instance.
     *
     * @param  Url  $url
     * @return void
     */
    public function __construct($url)
    {
        $this->url = $url;
        $this->search = Search::find($this->url->search_id);
        $this->backup = $this->search->backups()
        ->first();
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        // Send user notification of failure, etc...
    }

    /**
     * Execute the job.
     *
     * @param  Url  $url
     * @return void
     */
    public function handle()
    {
        Log::info("ProcessUrl handle: ". $this->url->name);
        $this->process();

    }

    public function process() {

        if ($this->validateUrl()) {
            // Use ReactCurl or PhantomJs depending on the file type
            if ($this->isDownloadable($this->url->name)) {
                $content = $this->getDocumentWithCurl();
            }
            else {
                $content = $this->getDocumentWithPhantom();
            }
            $this->uploadToS3();
            Log::info("ProcessUrl Crawled Success: ". $this->url->name);
        }
    }

    /**
     * Upload page to S3
     *
     * @return bool
     */
    protected function uploadToS3()
    {
        $aws_config = config('aws');

        /*1. Instantiate the client.*/
        $s3 = S3Client::factory($aws_config);
        $domain = $this->search->entrypoint;

        $remote_url = $this->backup->remote_url;
        $bucket = \App\Helpers\My::getBucketFromDomain($remote_url);

        $sanitize_domain = trim($this->url->name);
        $path = str_replace($domain, '', $sanitize_domain);
        
        if ($path == "" || $path == "/") {
            $path = "index.html";
        }
        $parts = explode( '?', $path ); 
        $filename = $parts[0];
        echo "Creating a new page with key {$filename} on {$bucket}\n";
        $s3->putObject([
            'Bucket' => $bucket,
            'Key'    => $filename,
            'Body'   => str_replace($domain, $remote_url, $this->url->page_html),
            'ACL' => 'public-read',
            'ContentType' => 'text/html',
            ]);
        $this->url->uploaded = true;
        $this->url->save();
    }

    /**
     * Check if the search has been deleted or marked as finished.
     *
     * @return bool
     */
    protected function searchIsDeletedOrFinished()
    {
        $search = $this->search;

        if ($search && $search->finished != true) {
            return false;
        }

        return true;
    }

    /**
     * Check if the url is crawlable
     *
     * @return bool
     */
    protected function validateUrl() {

        $parser = new RobotsTxtParser($this->search->robots);
        $parser->setUserAgent('xxx');

        if ($parser->isDisallowed($this->url->name)) {
            Log::info("ProcessUrl validateUrl isDisallowed: ". $this->url->name);
            return false;
        }
        
        // Check if url is excluded
        if ($this->url->excluded == 1) {
            Log::info("ProcessUrl validateUrl Excluded: ". $this->url->name);
            return false;
        }
        // Check if url is already processed
        if ($this->url->crawled == 1 ) {
            Log::info("ProcessUrl validateUrl Already Crawled: ". $this->url->name);
            return false;
        }

         // check if the search has been deleted during the crawl process
        if ($this->searchIsDeletedOrFinished()) {
            Log::info("ProcessUrl validateUrl searchIsDeletedOrFinished: ". $this->url->name);
            return false;
        }

        if ($parser->isAllowed($this->url->name)) {
            Log::info("ProcessUrl validateUrl isAllowed: ".$this->url->name);
            return true;
        }
        return false;
    }

    /**
     * Check if a given url should be downloaded.
     *
     * @param string $url
     * @return bool
     */
    protected function isDownloadable($url)
    {
        return str_contains($url, [
            '.jpg',
            '.jp2',
            '.jpeg',
            // '.raw',
            '.png',
            '.gif',
            // '.tiff',
            '.bmp',
            '.svg',
            // '.fla',
            // '.swf',
            '.css',
            '.js',
            'xml',
            'font'
            ]);
    }

    public function reactCurl() {
        $loop = \React\EventLoop\Factory::create();
        $handler = new \WyriHaximus\React\GuzzlePsr7\HttpClientAdapter($loop);

        $client = new \GuzzleHttp\Client([
            'handler' => \GuzzleHttp\HandlerStack::create($handler),
            ]);
        Log::info($this->url->name . " => reactCurl processing: ");
        $client->getAsync($this->url->name)->then(function ($response) {
            // var_dump("github: ". time());
            // var_export($response, true);
            // var_export((string) $response->getBody());
            Log::info($this->url->name . " => reactCurl status: ".  var_export($response, true));

            if ($content !== false) {
                $this->url->update(['crawled' => true, 'image_data' => $content]);
            }
            else {
                Log::info(" => Failed job: ". $this->url->name . " => Imahge: ".  $response->getStatus()); 
            }

        });
    }

    /**
     * Download content using curl
     *
     * @param string $url
     * @return bool
     */
    function getDocumentWithCurl() {
        $ch = curl_init();
        $timeout = 45;
        $config['useragent'] = 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0';
        curl_setopt($ch, CURLOPT_USERAGENT, $config['useragent']);
        curl_setopt($ch, CURLOPT_REFERER, 'https://www.google.com/');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Cookie: test=cookie"));
        curl_setopt($ch, CURLOPT_URL, $this->url->name);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        $target = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        curl_close($ch);

        if ($data !== false) {
            $this->url->update(['crawled' => true, 'image_data' => $data, 'page_html' => $data]);
        }
        else {
            Log::info(" => Failed job: ". $this->url->name); 
        }
        return $data;
    }


    /**
    * Download content using PhantomJS
    *
    * @param string $url
    * @return bool
    */
    function getDocumentWithPhantom() {
        $client = Client::getInstance();

        $request = $client->getMessageFactory()->createRequest($this->url->name, 'GET');
        // $client->getEngine()->addOption('--load-images=true');
        // $client->isLazy(); // Tells the client to wait for all resources before rendering
        // $client->getEngine()->addOption('--ignore-ssl-errors=true');

        $request->setTimeout(10000);

        $response = $client->getMessageFactory()->createResponse();

        $client->send($request, $response);
        
        // $response->getContentType()
        Log::info($this->url->name . " => JS status: ".  $response->getStatus());
        if($response->getStatus() === 200) {
            $content = $response->getContent();
        }
        else {
            $content = "Couldnt crawl, Status: ".$response->getStatus();
        }

        if ($content !== false) {
            $this->url->update(['crawled' => true, 'page_html' => $content]);
        }
        else {
         Log::info(" => Failed job: ". $this->url->name . " => JS: ".  $response->getStatus()); 
     }

 }
}
