<?php

namespace App\Jobs;

use App\Image;
use App\Search;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
Use Log;

use t1gor\RobotsTxtParser\Source\RobotsTxtParser;
use vipnytt\UserAgentParser\UserAgentParser;
use Aws\S3\S3Client;

class ProcessImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;


    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 120;

    protected $image;
    protected $search;
    protected $backup;

    /**
     * Create a new job instance.
     *
     * @param  Image  $image
     * @return void
     */
    public function __construct($image)
    {
        $this->image = $image;
        $this->search = Search::find($this->image->search_id);
        $this->backup = $this->search->backups()->first();
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        // Send user notification of failure, etc...
    }

    /**
     * Execute the job.
     *
     * @param  Image  $image
     * @return void
     */
    public function handle()
    {
        $this->backup = $this->search->backups()->first();
        $this->process();
    }

    public function process() {
        $content = $this->getDocumentWithCurl();
        $this->uploadToS3();
        Log::info("ProcessImage process Crawled Successfully: ". $this->image->name);
    }

    /**
     * Upload page to S3
     *
     * @return bool
     */
    protected function uploadToS3()
    {
        $aws_config = config('aws');

        /*1. Instantiate the client.*/
        $s3 = S3Client::factory($aws_config);
        $domain = $this->search->entrypoint;

        $remote_url = $this->backup->remote_url;
        $bucket = \App\Helpers\My::getBucketFromDomain($remote_url);

        $sanitize_domain = trim($this->image->name);

        $path = str_replace($domain, '', $sanitize_domain);

        $parts = explode( '?', $path ); 
        $filename = $parts[0];
        echo "Creating a new image with key {$filename}\n";
        $s3->putObject([
            'Bucket' => $bucket,
            'Key'    => $filename,
            'Body'   => $this->image->image_data,
            'ACL' => 'public-read',
            'ContentType' => 'image/png',
            ]);
        $this->image->uploaded = true;
        $this->image->save();
    }


    /**
     * Check if the search has been deleted or marked as finished.
     *
     * @return bool
     */
    protected function searchIsDeletedOrFinished()
    {
        $search = $this->search;

        if ($search && $search->finished != true) {
            return false;
        }

        return true;
    }

    /**
     * Check if a given url should be downloaded.
     *
     * @param string $image
     * @return bool
     */
    protected function isDownloadable($image)
    {
        return str_contains($image, [
            '.jpg',
            '.jp2',
            '.jpeg',
            // '.raw',
            '.png',
            '.gif',
            // '.tiff',
            '.bmp',
            '.svg',
            // '.fla',
            // '.swf',
            '.css',
            '.js',
            'xml',
            'font'
            ]);
    }

    /**
     * Download content using curl
     *
     * @param string $image
     * @return bool
     */
    function getDocumentWithCurl() {
        $ch = curl_init();
        $timeout = 45;
        $config['useragent'] = 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0';
        curl_setopt($ch, CURLOPT_USERAGENT, $config['useragent']);
        curl_setopt($ch, CURLOPT_REFERER, 'https://www.google.com/');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Cookie: test=cookie"));
        curl_setopt($ch, CURLOPT_URL, $this->image->name);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        $target = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        curl_close($ch);

        if ($data !== false) {
            $this->image->update(['crawled' => true, 'image_data' => $data]);
        }
        else {
            Log::info(" => Failed job: ". $this->image->name); 
        }
        return $data;
    }

}
