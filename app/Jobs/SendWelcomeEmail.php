<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Mail\Mailer;
use Log;

class SendWelcomeEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $user;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        //
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        Log::info("Inside Queue User sendWelcomeEmail Request Cycle with Queues Begins");
        $mailer->send('email.welcome', ['data'=>'data'], function ($message) {

            $message->from('hessaoui@gmail.com', 'Test Queue');

            $message->to('c817f3b70d-185cb6@inbox.mailtrap.io');

        });
        Log::info("Inside Queue User sendWelcomeEmail Request Cycle with Queues Ends");
    }
}
