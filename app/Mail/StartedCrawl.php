<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class StartedCrawl extends Mailable
{
    use Queueable, SerializesModels;

    protected $search;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($search)
    {
        //
        $this->search = $search;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.startedcrawl')->with([
            'search' => $this->search,
        ]);
    }
}
