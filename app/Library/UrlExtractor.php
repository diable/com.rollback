<?php

namespace Hedii\Extractors;

use Symfony\Component\DomCrawler\Crawler as DomCrawler;

class UrlExtractor extends Extractor
{
    /**
     * An array of found urls.
     *
     * @var array
     */
    protected $urls;


    /**
     * An array of found urls with their type.
     *
     * @var array
     */
    protected $urlsType;
    /**
     * Extract the urls contained in the body of the provided dom.
     *
     * @param mixed $dom
     * @param string $url
     * @return array
     */

    public function extract($dom, $url)
    {
        // $regex = "(?:(?:https?:\/\/)([w-w]\w+)[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,5})";
        // $test_dom = str_replace('\/', '/', $dom);
        // preg_match_all($regex,
        //     $dom, $matches, PREG_PATTERN_ORDER);
        // var_dump($matches[0]);
        // die();
        $this->resetUrls();
        $crawler = new DomCrawler($dom, $url);


        $links_a = $crawler->filter('body a')->links();
        foreach ($links_a as $link) {
            if (!in_array($link->getUri(), $this->urls)) {
                $this->urls[] = $link->getUri();
                $this->urlsType['link'][] = $link->getUri();
            }
        }

        $links_css = $crawler->filter('link')->links();
        
        foreach ($links_css as $link) {
            if (!in_array($link->getUri(), $this->urls) && !$this->isMediaFile($link->getUri())) {
                $this->urls[] = $link->getUri();
                $this->urlsType['css'][] = $link->getUri();
            }
        }

        $links_js = $crawler->filter('script')->extract('src');

        foreach ($links_js as $link) {
            if (!empty($link) && !in_array($link, $this->urls)) {
                $this->urls[] = $link;
                $this->urlsType['js'][] = $link;
            }
        }

        $links_meta = $crawler->filter('meta')->extract('content');

        foreach ($links_meta as $link) {
            if (!empty($link) && !in_array($link, $this->urls)) {
                $this->urls[] = $link;
                $this->urlsType['meta'][] = $link;
            }
        }

        return $this->urls;
    }

    /**
     * Reset the urls array.
     *
     * @return void
     */
    private function resetUrls()
    {
        $this->urls = [];
    }

    /**
     * Reset the urls array.
     *
     * @return void
     */
    private function getUrlsType()
    {
        return $this->urlsType;
    }

    /**
     * Check if a given url is a media file url.
     *
     * @param string $url
     * @return bool
     */
    protected function isMediaFile($url)
    {
        return str_contains($url, [
            '.jpg',
            '.jp2',
            '.jpeg',
            '.png',
            '.gif',
            '.tiff',
            '.bmp',
            '.svg',
            '.swf',
            '.woff2',
            ]);
    }
}