<?php

namespace Hedii\Extractors;

use Symfony\Component\DomCrawler\Crawler as DomCrawler;

class ImageExtractor extends Extractor
{
    /**
     * An array of found urls.
     *
     * @var array
     */
    protected $urls;


    /**
     * An array of found urls with their type.
     *
     * @var array
     */
    protected $urlsType;
    /**
     * Extract the urls contained in the body of the provided dom.
     *
     * @param mixed $dom
     * @param string $url
     * @return array
     */

    public function extract($dom, $url)
    {
        $this->resetUrls();
        $crawler = new DomCrawler($dom, $url);
        

        $links_img = $crawler->filter('body img')->extract('src');
        foreach ($links_img as $link) {
            if (!in_array($link, $this->urls)) {
                $this->urls[] = $link;
                $this->urlsType['img'][] = $link;
            }
        }

        $links_css = $crawler->filter('head link')->links();
        
        foreach ($links_css as $link) {
            if (!in_array($link->getUri(), $this->urls) && $this->isMediaFile($link->getUri())) {
                $this->urls[] = $link->getUri();
                $this->urlsType['image'][] = $link->getUri();
            }
        }

        return $this->urls;
    }

    /**
     * Reset the urls array.
     *
     * @return void
     */
    private function resetUrls()
    {
        $this->urls = [];
    }

    /**
     * Reset the urls array.
     *
     * @return void
     */
    private function getUrlsType()
    {
        return $this->urlsType;
    }

    /**
     * Check if a given url is a media file url.
     *
     * @param string $url
     * @return bool
     */
    protected function isMediaFile($url)
    {
        return str_contains($url, [
            '.jpg',
            '.jp2',
            '.jpeg',
            '.png',
            '.gif',
            '.tiff',
            '.bmp',
            '.svg',
            '.swf',
            '.woff2',
            ]);
    }
}