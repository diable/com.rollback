<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{ 
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'forms';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type', 'action_url', 'page_url', 'form_method', 'form_payload', 'form_fields', 'user_id', 'search_id', 'synced', 'replied', 'active', 'version'];

    /**
     * Get the user that owns the image.
     */
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    /**
     * Get the search that owns the image.
     */
    public function search()
    {
    	return $this->belongsTo(Search::class);
    }

}
