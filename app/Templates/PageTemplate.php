<?php 

namespace App\Templates;

use TCG\Voyager\Models\Page;
use TCG\Voyager\Models\Post;
use Carbon\Carbon;
use Illuminate\View\View;

class PageTemplate extends AbstractTemplate
{
    protected $view = 'marketing.page';

    protected $page;

    public function __construct(Page $page)
    {
        $this->page = $page;
    }

    public function prepare(View $view, array $parameters)
    {
        $page = $this->page->where('id', $parameters['id'])->where('slug', $parameters['slug'])->first();
        $view->with('page', $page);
    }
}
