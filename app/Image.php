<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'images';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'crawled', 'user_id', 'search_id', 'image_data'];

    /**
     * Get the user that owns the image.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the search that owns the image.
     */
    public function search()
    {
        return $this->belongsTo(Search::class);
    }

    /**
     * Encode utf8 page_html.
     *
     * @param  string  $value
     * @return string
     */
    public function getImageDataAttribute($value, $mime="image/*")
    {
          $base64   = base64_encode($value); 
          return ('data:' . $mime . ';base64,' . $base64);
        // return ($value);
    }
}
