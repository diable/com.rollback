<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Log;

class CrawlerIsDone
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $search;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($search)
    {
        Log::info("CrawlerIsDone __construct Request Cycle with Queues Begins");
        $this->search = $search;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
