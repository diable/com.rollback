<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Url;
use Illuminate\Support\ServiceProvider;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobProcessing;
use Illuminate\Support\Facades\Queue;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        View::share('currentRoute', url()->current());
        Queue::before(function (JobProcessing $event) {
                      // $event->connectionName
                      // $event->job
                      // $event->job->payload()
                      });
        
        Queue::after(function (JobProcessed $event) {
                     // $event->connectionName
                     // $event->job
                     // $event->job->payload()
                     });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
