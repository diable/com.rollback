<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MarketingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../../config/marketing.php' => config_path('marketing.php'),
            __DIR__.'/../../resources/views' => resource_path('views/vendor/bow'),
            __DIR__.'/../../database/seeds' => base_path('database/seeds'),
        ], 'marketing');

        $this->loadRoutesFrom(__DIR__.'/../../routes/marketing.php');

        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'marketing');

        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../../config/marketing.php',
            'marketing'
        );
    }
}
