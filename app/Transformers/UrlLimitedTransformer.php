<?php

namespace App\Transformers;

use App\Url;
use League\Fractal\TransformerAbstract;

class UrlLimitedTransformer extends TransformerAbstract
{

    public function transform(Url $url)
    {
        // $page_html = explode("</head>", $url->page_html);
        return [
            'id' => (int) $url->id,
            'name' => $url->name,
            'uploaded' => $url->uploaded,
            'crawled' => $url->crawled,
            // 'page_head' => $page_html[0]."</head>",
            'links' => [
                'self' => url('api/users/' . $url->user_id . '/searches/' . $url->search_id . '/urls/' . $url->id)
            ],
            'related' => [
                'user' => [
                    'links' => [
                        'self' => url('api/users/' . $url->user_id),
                    ]
                ],
                'search' => [
                    'links' => [
                        'self' => url('api/users/' . $url->user_id . '/searches/' . $url->search_id),
                    ]
                ]
            ]
        ];
    }
}