<?php

namespace App\Transformers;

use App\Backup;
use League\Fractal\TransformerAbstract;

class BackupTransformer extends TransformerAbstract
{
    /**
     * @param Backup $backup
     * @return array
     */
    public function transform(Backup $backup)
    {
        return [
            'id' => (int) $backup->id,
            'resource_type' => $backup->type,
            'entrypoint_url' => $backup->entrypoint,
            'remote_url' => $backup->remote_url,
            'class' => 'BackupTransformer',
            'finished' => (bool) $backup->finished,
            'created_at' => (string) $backup->created_at,
            'updated_at' => (string) $backup->updated_at,
            'version' => (string) $backup->version,
            'links' => [
                'self' => url('api/users/' . $backup->user_id . '/searches/' . $backup->id),
            ],
            'related' => [
                'user' => [
                    'links' => [
                        'self' => url('api/users/' . $backup->user_id),
                    ]
                ],
                'urls' => [
                    'links' => [
                        'self' => url('api/users/' . $backup->user_id . '/searches/' . $backup->id . '/urls')
                    ],
                    'data' => $this->getUrlsStats($backup)
                ],
                'resources' => [
                    'links' => [
                        'self' => url('api/users/' . $backup->user_id . '/searches/' . $backup->id . '/resources')
                    ],
                    'data' => $this->getResourcesStats($backup)
                ]
            ],
            'urls' => $backup->urls
        ];
    }

    private function getUrlsStats(Backup $backup)
    {
        $search = $backup->search()->remember(10)->get();
        $search = $search[0];
        $urlTotal = $search->urlsCount;
        $urlCrawled = $search->crawledUrlsCount;

        if ($urlTotal > 0) {
            $urlPercentCrawled = round(($urlCrawled / $urlTotal) * 100);
        } else {
            $urlPercentCrawled = 0;
        }
        $urlNotCrawled = 100 - $urlPercentCrawled;


        $urlUploaded = $search->uploadedUrlsCount;

        if ($urlTotal > 0) {
            $urlPercentUploaded = round(($urlUploaded / $urlTotal) * 100);
        } else {
            $urlPercentUploaded = 0;
        }
        $urlNotUploaded = 100 - $urlPercentUploaded;

        return [
            'total' => $urlTotal,
            'crawled' => $urlCrawled,
            'uploaded' => $urlUploaded,
            'percent_crawled' => $urlPercentCrawled,
            'percent_not_crawled' => $urlNotCrawled,
            'percent_uploaded' => $urlPercentUploaded,
            'percent_not_uploaded' => $urlNotUploaded
        ];
    }

    private function getResourcesStats(Backup $backup)
    {
        $search = $backup->search()->remember(10)->get();
        $search = $search[0];
        return [
            'total' => $search->resourcesCount
        ];
    }
}