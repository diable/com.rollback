<?php

namespace App\Crawler;

use App\Search;
use Hedii\Extractors\Extractor;
use JonnyW\PhantomJs\Client;
use Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use App\Events\CrawlerIsDone;
use Log;
use App\Jobs\JobCrawler;;
use App\Jobs\ProcessUrl;
use App\Events\SavedUrl;
use App\Jobs\ProcessImage;
use App\Events\SavedImage;
use DB;
use App\GoogleSitemapParser;

class Crawler
{
    /**
     * The search Eloquent model.
     *
     * @var \App\Search
     */
    protected $search;

    /**
     * The search's domain name.
     *
     * @var string
     */
    protected $domainName;

    /**
     * @var \Hedii\Extractors\Extractor
     */
    protected $extractor;

    /**
     * Crawler constructor.
     *
     * @param \Hedii\Extractors\Extractor $extractor
     */
    public function __construct(Extractor $extractor)
    {
        $this->extractor = $extractor;
    }

    /**
     * Run the crawler until the search is finished or until
     * the search is deleted.
     *
     * @param \App\Search $search
     * @return bool
     */
    public function run(Search $search)
    {
        $this->search = $search;
        $sitemap_urls = [];
        $sitemap_path = $search->entrypoint.'sitemap.xml';
        Log::info($sitemap_path);

        if (!filter_var($sitemap_path, FILTER_VALIDATE_URL)) {
            Log::info('Passed URL not valid according to filter_var function');
            die("error GoogleSitemapParser");
        }
        $sitemap = $this->getDocumentWithCurl($sitemap_path);

        try {
            $posts = (new GoogleSitemapParser($sitemap_path, false, $sitemap))->parse();
            Log::info("Cposts: ". var_export($posts, true));
            foreach ($posts as $post) {
                $sitemap_urls[] = $post;
            }
            $this->storeUrls($sitemap_urls);
        } catch (GoogleSitemapParserException $e) {
            print $e->getMessage();
        }


        $robots = $this->getDocumentWithCurl($search->entrypoint.'robots.txt');
        
        
        DB::table('searches')
        ->where('id', $search->id)
        ->update(['sitemap' => $sitemap,
            'robots' => $robots]);

        $this->domainName = $this->getDomainName($this->search->entrypoint);
        Log::info("Crawler is running for: ". $this->domainName);


        // crawl search's entrypoint url
        $this->crawl($this->search->entrypoint, true);

        // Running twice;
        // crawl all search's urls
        foreach ($this->search->urls() as $key => $url) {
            $this->crawl($url);

            // check if the search has been deleted during the crawl process
            if ($this->searchIsDeletedOrFinished()) {
                break;
            }
        }

        foreach ($this->search->images() as $key => $media_url) {
            $this->crawl($media_url);

            // check if the search has been deleted during the crawl process
            if ($this->searchIsDeletedOrFinished()) {
                break;
            }
        }
        // while ($url = $this->getNextNotCrawledUrl()) {
        //     $this->crawl($url);

        //     // check if the search has been deleted during the crawl process
        //     if ($this->searchIsDeletedOrFinished()) {
        //         return false;
        //     }
        // }

        // // crawl all search's media urls
        // while ($media_url = $this->getNextNotCrawledMedia()) {
        //     $this->crawlMedia($media_url);

        //     // check if the search has been deleted during the crawl process
        //     if ($this->searchIsDeletedOrFinished()) {
        //         return false;
        //     }
        // }

        
        // this search is finished!
        // $this->search->update(['finished' => true]);
        // var_dump("// this search is finished!");
        return false;
    }

    /**
     * Crawl an url and extract resources.
     *
     * @param mixed $url
     * @param bool $entrypoint
     */
    protected function crawlMedia($image)
    {
        $cleanUrl = $this->normalizeUrl($image->name);
        var_dump("image:". $image->name . " => " .$cleanUrl);
        $content = $this->getDocumentWithCurl($cleanUrl);

        if ($content !== false && !empty($content)) {
            $image->update(['crawled' => true, 'image_data' => $content]);
        }
    }

    /**
     * Crawl an url and extract resources.
     *
     * @param mixed $url
     * @param bool $entrypoint
     */
    protected function crawl($url, $entrypoint = false)
    {
        if ($entrypoint) {
            $cleanUrl = $this->normalizeUrl($url);
            
            $resources = $this->extractor->searchFor(['urls', 'emails', 'images'])
            ->at($cleanUrl)
            ->get();

            $this->storeUrls($resources['urls']);
            $this->storeMedia($resources['images']);
            $this->storeEmails($resources['emails']);

        }
    }

    /**
     * Store urls in the database.
     *
     * @param $urls
     * @return $this
     */
    protected function storeUrls($urls)
    {
        if (count($urls) > 0) {
            foreach ($urls as $url) {

                $url = $this->normalizeUrl($this->cleanUrl($url));

                if (
                    // // if url is not a valid url, continue
                    (!$this->isValidUrl($url)) ||
                    // // or, if domainLimit, get only the same domain urls
                    ($this->search->domain_limit && ($this->getDomainName($url) !== $this->domainName)) ||
                    // we don't want media files like images
                    $this->isMediaFile($url)
                    ) {
                    continue;
            }

            $url = $this->search->urls()->firstOrCreate([
                'name' => $url,
                'user_id' => $this->search->user_id
                ]);
            Log::info("in storeUrls");
            event($url);
            dispatch(new ProcessUrl($url));
        }
    }

    return $this;
}

    /**
     * Store media urls in the database.
     *
     * @param $urls
     * @return $this
     */
    protected function storeMedia($images)
    {
        if (count($images) > 0) {
            foreach ($images as $image) {
                $image = $this->normalizeUrl($this->cleanUrl($image));

                if (
                    // if url is not a valid url, continue
                    (!$this->isValidUrl($image)) ||
                    // or, if domainLimit, get only the same domain images
                    ($this->search->domain_limit && ($this->getDomainName($image) !== $this->domainName))
                    ) {
                    continue;
            }

            $image = $this->search->images()->firstOrCreate([
                'name' => $image,
                'user_id' => $this->search->user_id
                ]);
            Log::info("in storeMedia");
            event($image);
            dispatch(new ProcessImage($image));
        }
    }

    return $this;
}

    /**
     * Store emails in the database.
     *
     * @param $emails
     * @return $this
     */
    protected function storeEmails($emails)
    {
        if (count($emails) > 0) {
            foreach ($emails as $email) {
                if (!$this->isValidEmail($email)) {
                    continue;
                }

                $this->search->resources()->firstOrCreate([
                    'type' => $this->search->type,
                    'name' => $email,
                    'user_id' => $this->search->user_id
                    ]);
            }
        }

        return $this;
    }

    /**
     * Check if the search has been deleted or marked as finished.
     *
     * @return bool
     */
    protected function searchIsDeletedOrFinished()
    {
        $search = Search::find($this->search->id);

        if ($search && $search->finished != true) {
            return false;
        }

        return true;
    }

    /**
     * Get the search's url that has not been crawled yet.
     *
     * @return mixed
     */
    protected function getNextNotCrawledUrl()
    {
        return $this->search->urls()
        ->where('crawled', '0')
        ->first();
    }

    /**
     * Get the search's media url that has not been crawled yet.
     *
     * @return mixed
     */
    protected function getNextNotCrawledMedia()
    {
        return $this->search->images()
        ->where('crawled', '0')
        ->first();
    }

    /**
     * Get the search's media url that has not been crawled yet.
     *
     * @return mixed
     */
    protected function getNextNotCrawledMedias()
    {
        return $this->search->images()
        ->where('crawled', '0')->get();
    }


    /**
     * A wrapper for php parse_url host function.
     *
     * @param string $url
     * @return mixed
     */
    protected function getDomainName($url)
    {
        return parse_url($url, PHP_URL_HOST);
    }

    /**
     * A wrapper for php filter_var url function.
     *
     * @param string $url
     * @return bool
     */
    protected function isValidUrl($url)
    {
        return filter_var($url, FILTER_VALIDATE_URL) !== false;
    }

    /**
     * A wrapper for php filter_var email function.
     *
     * @param string $email
     * @return bool
     */
    protected function isValidEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    }

    /**
     * Remove unwanted character at the end of the url string,
     * and remove anchors in the url.
     *
     * @param string $url
     * @return string
     */
    protected function cleanUrl($url)
    {
        $url = rtrim(rtrim($url, '#'), '/');

        if (!empty(parse_url($url, PHP_URL_FRAGMENT))) {
            $url = str_replace('#' . parse_url($url, PHP_URL_FRAGMENT), '', $url);
        }

        return rtrim($url, '?');
    }

    /**
     * Check if a given url is a media file url.
     *
     * @param string $url
     * @return bool
     */
    protected function isMediaFile($url)
    {
        return str_contains($url, [
            // '.jpg',
            // '.jp2',
            // '.jpeg',
            '.raw',
            // '.png',
            // '.gif',
            '.tiff',
            '.bmp',
            // '.svg',
            '.fla',
            '.swf',
            // '.xml',
            '.json',
            // '.woff2',
            'xmlrpc.php'
            // '.css',
            // '.js'
            ]);
    }

    /**
     * Check if a given url should be downloaded.
     *
     * @param string $url
     * @return bool
     */
    protected function isDownloadable($url)
    {
        return str_contains($url, [
            '.jpg',
            '.jp2',
            '.jpeg',
            // '.raw',
            '.png',
            '.gif',
            // '.tiff',
            '.bmp',
            '.svg',
            // '.fla',
            // '.swf',
            '.css',
            '.js'
            ]);
    }

    /**
     * Check if URL is valid
     * Add domain and protocol to URL
     *
     * @param string $url
     * @return bool
     */
    function normalizeUrl($url) {
        $cleanUrl = false;
        if ($this->isValidUrl($url)) {
            $cleanUrl = $url;
        }
        else {
            $rebuild = parse_url($url);
            $rebuild_ct = count($rebuild);
            if ($rebuild_ct == 1 && isset($rebuild['path'])) {
                $cleanUrl = $this->domainName.$rebuild['path'];
            }
            if (isset($rebuild['schema']) && isset($rebuild['host'])) {
                $cleanUrl = $url;
            }
            if (!isset($rebuild['schema']) && isset($rebuild['host'])) {
                $cleanUrl = "http:".$url;
            }
        }
        return $cleanUrl;
    }

    /**
     * Download content using curl
     *
     * @param string $url
     * @return bool
     */
    function getDocumentWithCurl($url) {
        $ch = curl_init();
        $timeout = 45;
        $config['useragent'] = 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0';
        curl_setopt($ch, CURLOPT_USERAGENT, $config['useragent']);
        curl_setopt($ch, CURLOPT_REFERER, 'https://www.google.com/');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Cookie: test=cookie"));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        $target = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        curl_close($ch);
        return $data;
    }

}