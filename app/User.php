<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Watson\Rememberable\Rememberable;

class User extends Authenticatable
{
    use Billable;
    use Rememberable;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'email_token', 'verified',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the searches for user.
     */
    public function searches()
    {
        return $this->hasMany(Search::class);
    }

    /**
     * Get the urls for user.
     */
    public function urls()
    {
        return $this->hasMany(Url::class);
    }

    /**
     * Get the images for user.
     */
    public function images()
    {
        return $this->hasMany(Image::class);
    }

    /**
     * Get the settings for user.
     */
    public function settings()
    {
        return $this->hasOne(Setting::class);
    }

    /**
     * Get the resources for user.
     */
    public function resources()
    {
        return $this->hasMany(Resource::class);
    }

    /**
     * Get the backups for user.
     */
    public function backups()
    {
        return $this->hasMany(Backup::class);
    }
}
