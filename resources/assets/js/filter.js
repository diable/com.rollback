//
// filters.js
//

function filterBy(list, value) {
	return list.filter(function(item) {
		return item.indexOf(value) > -1;
	});
}

function findBy(list, value) {
	return list.filter(function(item) {
		return item == value
	});
}

function reverse(value) {
	return value.split('').reverse().join('');
}

function showStatus(value) {
    if (value === false || value == 0) {
        return  ('<i class="fa fa-spinner fa-spin"></i><span class="hidden-xs">'+value+' Running</span>');
    }
    return  ('<i class="fa fa-check"></i><span class="hidden-xs">'+value+' Finished</span>');
}

function htmlEscape(str) {
    return str
        .replace(/&/g, '&amp;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#39;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;');
}

// I needed the opposite function today, so adding here too:
function htmlUnescape(str){
    return str
        .replace(/&quot;/g, '"')
        .replace(/&#39;/g, "'")
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>')
        .replace(/&amp;/g, '&');
}
// function formatInteger(value) {
// 	return value.toLocaleString();
// }

// function timeAgo(value) {
// 	var moment = require('moment');
// 	return moment(value).fromNow();
// }

export {filterBy, reverse, findBy, showStatus}