require('./bootstrap');
var filters = require('./filter');
var moment = require('moment');

var Vue = require('vue');
window.Vue = Vue;
var Resource = require('vue-resource');
Vue.use(Resource);
Vue.config.debug = true;
Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');


import {filterBy, reverse, findBy} from './filter.js'

import BackupIndex from './components/BackupIndex.vue';
import BackupShow from './components/BackupShow.vue';
import BackupActionsButtons from './components/BackupActionsButtons.vue';

import SearchIndex from './components/SearchIndex.vue';
import SearchCreate from './components/SearchCreate.vue';
import SearchShow from './components/SearchShow.vue';
import SearchActionsButtons from './components/SearchActionsButtons.vue';

import SettingsIndex from './components/SettingsIndex.vue';
import ProfileIndex from './components/ProfileIndex.vue';

if (
    document.body.classList.contains('backups-index') ||
    document.body.classList.contains('backups-show') ||
    document.body.classList.contains('settings-index') ||
    document.body.classList.contains('profile-index') ||
    document.body.classList.contains('searches-index') ||
    document.body.classList.contains('searches-show') ||
    document.body.classList.contains('searches-create')
    ) {


    Vue.filter('formatInteger', function (value) {
        return value.toLocaleString();
    });

    Vue.filter('timeAgo', function (value) {
        return moment(value).fromNow();
    });

    new Vue({
        el: '#app',
        data: {},
        methods: {
            
        },
        filters: {
          allCaps: function (value) {
            if (!value) return '';
            value = value.toString();
            return value.toUpperCase();
          },
        },
        computed: {
            
        },
        components: {
            SearchIndex,
            SearchCreate,
            SearchShow,
            BackupIndex,
            BackupShow,
            SettingsIndex,
            ProfileIndex
        }
    });

}

