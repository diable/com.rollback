<div class="container">
	<h1>Blog Overview</h1>

	<div class="row">
		@foreach($posts as $post)
		<div class="col-md-3">
			<a href="/blog/{{ $post->slug }}">
				<img src="{{ Voyager::image( $post->image ) }}" style="width:100%">
				<span>{{ $post->title }}</span>
			</a>
		</div>
		@endforeach
	</div>
	<div>
		{!! $posts->links() !!}
	</div>
</div>
