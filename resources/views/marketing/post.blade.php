<div class="container">

	<div class="row">
		<h1>Single Post</h1>

		<h1>/resources/views/marketing/post.blade.php</h1>
		<h1>{{ $post->title }}</h1>
		<img src="{{ Voyager::image( $post->image ) }}" style="width:100%">
		<p>{!! $post->body !!}</p>
	</div>
	
</div>