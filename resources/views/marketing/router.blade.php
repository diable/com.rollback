
<h2>Content Page.blade.php</h2>
@if($page->view)
<h2>View Page.blade.php</h2>
<div class="container">
{!! $page->view->render() !!}
</div>
@else
<h2>No view Page.blade.php</h2>
<div class="container">

  <div class="starter-template">
    <h1>{{ $page->title }}</h1>
    {!! $page->body !!}
</div>

</div><!-- /.container -->
@endif

