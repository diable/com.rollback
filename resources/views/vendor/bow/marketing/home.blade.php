<!-- Content -->
<div class="container" id="features">
  <!-- Section -->
  <div class="section">
    <h2>Just a few of Voyagers Features</h2>
    <!-- Tabs  -->
    <div id="tabs">
      <ul>
        <li>
          <h3><a href="#tabs-1">
            <span>^</span>
            Media Manager
          </a></h3>
        </li>
        <li>
          <h3><a href="#tabs-2">
            <span>&#xe027;</span>
            Menu Builder
          </a></h3>
        </li>
        <li>
          <h3><a href="#tabs-3">
            <span>&#xe001;</span>
            Database Manager
          </a></h3>
        </li>
        <li>
          <h3><a href="#tabs-4">
            <span>&#xe025;</span>
            BREAD/CRUD Builder
          </a></h3>
        </li>
      </ul>

      <!-- Tab 1 -->
      <div class="indv-tab scrollme" id="tabs-1">
        <div class="col-md-6 img-preview animateme" data-when="enter" data-from="1" data-to="0.15" data-translatex="-400">
          <img src="/frontend/images/mock.png" alt="Voyager Perspective Mockup">
        </div>
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-md-push-6">
              <h4 class="left">Fully Functional Media Manager</h4>
              <p>Voyager has an amazing fully function media manager which allows you to view/edit/delete files from your storage. All files in your application will be easily accessible and will live in a single place. Compatible with local or s3 file storage.</p>
            </div>
          </div>
        </div>
      </div>

      <!-- Tab 2 -->
      <div class="indv-tab scrollme" id="tabs-2">
        <div class="col-md-6 img-preview animateme" data-when="enter" data-from="1" data-to="0.15" data-translatex="-400">
          <img src="/frontend/images/mock2.png" alt="Voyager page">
        </div>
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-md-push-6">
              <h4 class="left">Menu Builder</h4>
              <p>You can easily build as many menus for your site. In fact the menu in the voyager admin is built using the menu builder. You can add/edit/delete menu items from as many menus as you would like. Then it's super easy to display your menu on the front-end of your app <code>Menu::display('main')</code></p>
            </div>
          </div>
        </div>
      </div>

      <!-- Tab 3 -->
      <div class="indv-tab scrollme" id="tabs-3">
        <div class="col-md-6 img-preview animateme" data-when="enter" data-from="1" data-to="0.15" data-translatex="-400">
          <img src="/frontend/images/mock3.png" alt="Voyager page">
        </div>
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-md-push-6">
              <h4 class="left">Database Manager</h4>
              <p>That's right! You can actually manipulate your database directly from Voyager. You can add, edit, or delete tables. When creating a new table you can also specify if you would like voyager to create your Model for you... How cool is that.</p>
            </div>
          </div>
        </div>
      </div>

      <!-- Tab 4 -->
      <div class="indv-tab scrollme" id="tabs-4">
        <div class="col-md-6 img-preview animateme" data-when="enter" data-from="1" data-to="0.15" data-translatex="-400">
          <img src="/frontend/images/mock4.png" alt="Voyager page">
        </div>
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-md-push-6">
              <h4 class="left">BREAD/CRUD Builder</h4>
              <p>Similar to CRUD, Voyager has a system called BREAD which is Browse, Read, Edit, Add, and Delete. You can easily add any BREAD views and functionality to any table in your database. Have a products table, easily add the Browse Read Edit Add & Delete functionality in seconds!</p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

  <!-- Section -->
  <div class="section" id="demo" style="margin-top:0px;">
    <h2>See it in action</h2>
    <div class="row">
      <div class="col-md-10 col-md-push-1 scrollme">
        <p class="italic centered">Checkout this short video where you can get a quick glimpse of Voyager and how easy it is to install.</p>
        <a type="button" class="btn btn-primary centered" style="max-width:300px;" href="https://www.youtube.com/watch?v=RSAnupACbhg&list=PL_UnIDIwT95PEQFNdgXZGo5SYU5V_TQvc" target="_blank">View On Youtube</a>
        <p class="centered margin-top no-margin-bottom animateme" data-when="enter" data-from="1" data-to="0.15" data-translatey="600"><iframe id="video" src="https://www.youtube.com/embed/RSAnupACbhg?list=PL_UnIDIwT95PEQFNdgXZGo5SYU5V_TQvc" frameborder="0" allowfullscreen></iframe></p>
      </div>
    </div>
  </div>

</div>


<!-- Section -->
<div class="grey">
  <div class="container">
    <div class="row">
      <!-- Icon block -->
      <div class="col-md-4 col-sm-6 icon scrollme animateme" data-when="enter" data-from=".7" data-to="0.15" data-opacity="0" data-scale="0" data-translatey="100">
        <span class="animateme" data-when="enter" data-from="1" data-to="0.15" data-rotatez="-90">c</span>
        <h5 class="no-underline">Add Custom Data Types</h5>
        <p>You can add custom Data Types to work with in your Laravel App.</p>
      </div>
      <!-- Icon block -->
      <div class="col-md-4 col-sm-6 icon scrollme animateme" data-when="enter" data-from=".7" data-to="0.15" data-opacity="0" data-scale="0" data-translatey="100">
        <span class="animateme" data-when="enter" data-from="1" data-to="0.15" data-rotatez="-90">&#xe005;</span>
        <h5 class="no-underline">Add Custom Settings</h5>
        <p>You can easily add custom settings in Voyager and use them on the front-end of your site.</p>
      </div>
      <!-- Icon block -->
      <div class="col-md-4 col-sm-6 icon scrollme animateme" data-when="enter" data-from=".7" data-to="0.15" data-opacity="0" data-scale="0" data-translatey="100">
        <span class="animateme" data-when="enter" data-from="1" data-to="0.15" data-rotatez="-90">&#xe013;</span>
        <h5 class="no-underline">Awesome Features</h5>
        <p>And just a whole bunch of awesome features you'll have to checkout for yourself!</p>
      </div>
    </div>
  </div>
</div>



