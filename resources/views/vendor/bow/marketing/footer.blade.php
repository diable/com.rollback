<!-- Section -->
<div class="section footer no-margin-top">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-5 copyright">
				<p>&copy; Voyager 2016. All rights reserved.<br />
					<a href="https://www.thecontrolgroup.com/" title="Voyager Package Built by The Control Group">Voyager Package Built by The Control Group</a>
				</p>
			</div>
			<!-- Social icons -->
			<div class="col-md-6 col-sm-7 social-icons">
				<ul>
					<li>
						<a href="http://twitter.com/ControlGroup_SD" target="_blank" class="socicon">a</a>
					</li>
					<li>
						<a href="http://facebook.com/TheControlGroup" target="_blank" class="socicon">b</a>
					</li>
					<li>
						<a href="https://plus.google.com/+TheControlGroupSanDiego" target="_blank" class="socicon">c</a>
					</li>
					<li>
						<a href="https://www.linkedin.com/company/the-control-group-media-company-inc-" target="_blank" class="socicon">j</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<script src="/frontend/js/jquery.js"></script>
<script src="/frontend/js/bootstrap.min.js"></script>
<script src="/frontend/js/headhesive.min.js"></script>
<script src="/frontend/js/jquery-ui.min.js"></script>
<script src="/frontend/js/scrollme.min.js"></script>
<script src="/frontend/js/matchHeight.min.js"></script>
<script src="/frontend/js/flickity.js"></script>
<script src="/frontend/js/bootstrap-dropdown-on-hover.js"></script>
<script src="/frontend/js/custom.js"></script>
<script>
  // $(document).on('click', '#menu-primary a.scroll', function(event){
  //   event.preventDefault();

  //   $('html, body').animate({
  //     scrollTop: $( $.attr(this, 'href') ).offset().top
  //   }, 500);
  // });
</script>
</body>
</html>