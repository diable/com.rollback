<div class="hero" id="home">
    <div id="bgdim"></div>
    <!-- Navigation -->
    <div class="navbar" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a href="/" title="Voyager Laravel Admin Package">
                    <img src="/frontend/images/logo.png" alt="Voyager Laravel Admin Package">
                </a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>  
            <div class="navbar-collapse collapse">
                <div class="collapse navbar-collapse">
                    <?php print str_replace("<ul>", '<ul id="menu-primary" class="nav navbar-nav">', menu('marketing')) ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Hero content -->
    <div class="container">
        <div class="row blurb">
            <div class="col-md-10 col-md-offset-1">
                <h1>The Missing Laravel Admin {{ $currentRoute }}</h1>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <p>Voyager is a Laravel Admin Package that includes BREAD(CRUD) operations,<br> a media manager, menu builder, and much more.</p>
                <a type="button" class="btn btn-default" href="https://github.com/the-control-group/voyager" target="_blank">View on Github</a> 
                <a type="button" class="btn btn-primary" href="docs/" target="_blank">Documentation</a>
            </div>
        </div>
        <div class="row preview">
            <div class="col-md-10 col-md-offset-1">
                <img src="/frontend/images/hero-app.png" alt="Voyager Admin Package" class="img-responsive">
            </div>
        </div>
    </div>
</div>

<!-- Section -->
<div class="section">
    @if($page->view)
    <div class="view-blade">
        {!! $page->view->render() !!}
    </div>
    @else

    <div class="starter-template no-view-blade">
        <h1>{{ $page->title }}</h1>
        {!! $page->body !!}
    </div>

    @endif

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Contact</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/form') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input type="name" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sign-in"></i>Send
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>