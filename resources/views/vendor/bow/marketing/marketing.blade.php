@include('marketing.header')

<!-- Homepage loading animation -->
    <div id="loader-wrapper">
      <img src="/frontend/images/helm.png">
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>


  @include('marketing.layout')


  @include('marketing.footer')

