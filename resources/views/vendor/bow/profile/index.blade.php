@extends('layouts.app')

@section('body-class'){{ 'profile-index' }}@stop

@section('content')
    <script>
        var userId = {{ Auth::user()->id }};
        var user = {!! json_encode(Auth::user()->toArray()) !!};
    </script>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">My profile</div>

                    <div class="panel-body" id="app">
                        <profile-index></profile-index>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
