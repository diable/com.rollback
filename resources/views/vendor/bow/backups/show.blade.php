@extends('layouts.app')

@section('body-class'){{ 'backups-show' }}@stop

@section('content')
<h1 class="page-title">
    <i class="backups"></i> Show Backup
    <a href="http://rollback.com.test/admin/backups/create" class="btn btn-success">
        <i class="voyager-plus"></i> Add New
    </a>
</h1>
<div class="page-content container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered">
                <div class="panel-heading">Search for {{ $backup->type }}s with entry point <a href="{{ $backup->entrypoint }}" target="_blank">{{ str_limit($backup->entrypoint, 50) }}</a></div>

                <div class="panel-body" id="app">
                    <backup-show></backup-show>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
