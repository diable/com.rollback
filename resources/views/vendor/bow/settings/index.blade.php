@extends('layouts.app')

@section('body-class'){{ 'settings-index' }}@stop

@section('content')
    <script>
        var userId = {{ Auth::user()->id }};
        var user = {!! json_encode(Auth::user()->toArray()) !!};
    </script>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">My Settings</div>

                    <div class="panel-body" id="app">
                        <settings-index></settings-index>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
