(function() {

// Localize jQuery variable
var jQuery;

/******** Load jQuery if not present *********/
if (window.jQuery === undefined || window.jQuery.fn.jquery !== '1.4.2') {
    var script_tag = document.createElement('script');
    script_tag.setAttribute("type","text/javascript");
    script_tag.setAttribute("src",
        "http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js");
    if (script_tag.readyState) {
      script_tag.onreadystatechange = function () { // For old versions of IE
          if (this.readyState == 'complete' || this.readyState == 'loaded') {
              scriptLoadHandler();
          }
      };
  } else {
      script_tag.onload = scriptLoadHandler;
  }
    // Try to find the head, otherwise default to the documentElement
    (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
} else {
    // The jQuery version on the window is the one we want to use
    jQuery = window.jQuery;
    main();
}

/******** Called once jQuery has loaded ******/
function scriptLoadHandler() {
    // Restore $ and window.jQuery to their previous values and store the
    // new jQuery in our local jQuery variable
    jQuery = window.jQuery.noConflict(true);
    // Call our main function
    main(); 
}

// The module pattern
var _revertly = (function() {

    // Private variables and functions
    var privateThing = "secret";
    var publicThing = "not secret";
    var api_url = "//revertly.com/api/";
    var domain = "examplecurrentdomain.com";

    var changePrivateThing = function() {
        privateThing = "super secret";
    };

    var sayPrivateThing = function() {
        console.log( privateThing );
        changePrivateThing();
    };

    var addJQuery = function() {

    }

    // Public API
    return {
        publicThing: publicThing,
        sayPrivateThing: sayPrivateThing
    };
})();

// _revertly.publicThing; // "not secret"

// // Logs "secret" and changes the value of privateThing
// _revertly.sayPrivateThing();

/******** Our main function ********/
function main() { 
    jQuery(document).ready(function($) { 
        /******* Load CSS *******/
        var css_link = $("<link>", { 
            rel: "stylesheet", 
            type: "text/css", 
            href: "style.css" 
        });
        css_link.appendTo('head');          

        /******* Load HTML *******/
        var jsonp_url = "http://rollback.com.test/api/users/1?callback=?";
      //   $.get(jsonp_url, function(data, status){

      //     //   var response = jQuery.parseJSON(data);
      //     // console.log(response);

      // });

        $.getJSON( jsonp_url, {
            // tags: "snowboarding",
            // tagmode: "any",
            // format: "json"
        })
        .done(function( data ) {
            $.each( data.items, function( i, item ) {
              // $( "<img/>" ).attr( "src", item.media.m ).appendTo( "#images" );
              // if ( i === 3 ) {
              //   return false;
            }
        });
        // $.getJSON(jsonp_url, function(data) {
        //   $('#example-widget-container').html("This data comes from another server: " );


        //   var response = jQuery.parseJSON(data);
        //   console.log(response);
           //  var head = "<thead class='thead-inverse'><tr>";
           //  $.each(response[0], function (k, v) {
           //      head = head + "<th scope='row'>" + k.toString() + "</th>";
           //  })
           //  head = head + "</thead></tr>";
           //  $(table).append(head);//append header
           // var body="<tbody><tr>";
           //  $.each(response, function () {
           //      body=body+"<tr>";
           //      $.each(this, function (k, v) {
           //          body=body +"<td>"+v.toString()+"</td>";                                        
           //      }) 
           //      body=body+"</tr>";               
           //  })
           //  body=body +"</tbody>";
           //  $(table).append(body);//append body
            // });
        // $('#example-widget-container').html("This data comes from another server: ");
    });
}

})(); // We call our anonymous function immediately


// Listly.listlyReady = function () {
//   // Check for presence of required DOM elements or other JS your widget depends on
//   if (Listly.jQuery.listlybox && ListlyAuth) {
//     window.clearInterval(Listly._readyInterval);
//     // Make stuff here
//   }
// };

// // This our new main function
// function main () {
//   Listly._readyInterval = window.setInterval(Listly.listlyReady, 500);
// }
